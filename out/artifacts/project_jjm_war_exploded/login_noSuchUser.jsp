<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@include file="style/style_head.jsp" %>
    <link rel="stylesheet" href="style/css/main_styling.css" type="text/css">
    <title>Login</title>
</head>
<body>

<%
    session.setAttribute("erase_mode", "false");

%>

<%@include file="navbar.jsp" %>
<div class="container-fluid" id="login-page">
    <div class="container-fluid" id="login-float">

        <div>

            <h2 style="color: red">USER NAME DOESN'T EXIST</h2>

        </div>

        <div>
            <form class="form-horizontal" action="/Login" method="post">
                <fieldset>
                    <legend class="legend"><strong>Login.</strong></legend>
                    <hr>
                    <div class="row">

                        <div class="form-group col-md-6">
                            <label class="control-label" for="username">Username</label>
                            <div class="input col-md-12">
                                <input type="text" id="username" placeholder="Your User Name" name="username">
                            </div>
                        </div>

                        <div class="form-group col-md-6">
                            <label class="control-label" for="pass1">Password</label>
                            <div class="input col-md-12">
                                <input type="password" class="form-control" id="pass1" placeholder="Enter Your Password"
                                       name="password">
                            </div>
                        </div>

                        <div class="form-group col-md-6">
                            <div class="input col-md-12">
                                <button type="submit" id="submit" name="password" class="btn btn-warning">Login</button>
                                <%--<input type="submit" class="form-control" id="submit" placeholder="Enter Your Password"--%>
                                       <%--name="password">--%>
                            </div>
                        </div>

                    </div>
                </fieldset>

            </form>
        </div>
    </div>
</div>

</body>
</html>
