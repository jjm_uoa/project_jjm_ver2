<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: macbookpro
  Date: 23/05/18
  Time: 10:42 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@include file="style/style_head.jsp" %>

    <link rel="stylesheet" href="style/css/main-style.css" type="text/css">
    <script rel="script" href="style/js/comment-script.js"></script>

    <title>${article.title}</title>

</head>
<body>
<%@include file="navbar.jsp" %>

<div class="container-fluid" id="article-view-page">
    <div class="container-fluid" id="article-view-float">
        <div class="row">
            <div class="card col-md-12 col-xs-12">
                <div class="card-body">
                    <form action="/modify" method="get">
                        <div id="article-title-div">
                            <h1 class="card-title" id="article-title">${article.title}</h1>
                        </div>

                        <hr>
                        <p class="user-info">${article.fname}&nbsp;${article.lname}&nbsp;//&nbsp;${article.time}</p>
                        <hr>
                        <div id="article-content-div">
                            <p class="card-text" id="article-content">${article.content}</p>
                        </div>
                        <div id="submit-changes"></div>
                        <hr>
                    </form>

                    <%--<c:if test="${sessionScope.login != null}">--%>
                    <%--users can add a comment on any article--%>
                    <div class="row">
                        <div id="control-buttons">
                            <form action="/deleteArticle" method="get">
                                <c:if test="${sessionScope.login != null}">
                                    <button id="comment-button" class="btn btn-warning" onclick="addCommentBox()"><i
                                            class="fas fa-comment"></i>
                                    </button>

                                    <c:if test="${sessionScope.id == sessionScope.articleUserID}">
                                        <%--users, if article belongs to them, they are able to modify and delete article, and delte comments--%>
                                        <button id="modify-button" class="btn btn-success"
                                                onclick="modifyArticleButton()">
                                            <i class="fas fa-wrench"></i>
                                        </button>
                                        <button type="submit" class="btn btn-danger delete-article"><i
                                                class="fas fa-minus-circle"></i></button>
                                    </c:if>
                                </c:if>
                            </form>
                        </div>


                    </div>
                    <hr>
                </div>
            </div>
        </div>


        <form action="/comments" method="get" class="form-horizontal">
            <fieldset>
                <div class="row" style="padding-left: 5px">
                    <div class="form-group col-md-12">
                        <div class="col-md-12" id="comment-div">
                            <!-- text box created here -->
                        </div>
                    </div>
                </div>
                <%--for testing--%>
                <%--<input type="text" name="comment">--%>
                <%--<input type="submit" class="btn btn-primary">--%>

                <div class="row" id="add-comment-buttons">
                    <div class="form-group">
                        <form action="articleView.jsp" method="get">
                            <div id="submit-button"></div>
                        </form>
                    </div>
                    <div class="form-group">
                        <div id="cancel-button" style="margin-left: 10px"></div>
                    </div>
                </div>
            </fieldset>
        </form>

        <div class="row" id="comment-header">
            <p><strong>Comments.</strong></p>
        </div>

        <%--${comments}--%>
        <div class="row">
            <c:forEach var="comment" items="${existComments}">
                <div class="container-fluid col-md-12 comment-body">
                    <div id="comment-text">
                        <form action="/delete" method="get">
                            <p id="comment">${comment.comment}

                                    <%--users, if comments belong to them, are able to delete on own and others articles--%>
                                <c:if test="${sessionScope.login != null }">
                                    <c:if test="${sessionScope.login == comment.userName || sessionScope.id == sessionScope.articleUserID}">
                                        <button type="submit" class="close" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </c:if>
                                </c:if>
                            </p>
                            <p id="comment-info">${comment.userName}&nbsp;//&nbsp;${comment.commentCreated}</p>
                            <input type="hidden" name="thisCommentID" value=${comment.commmentId}>
                        </form>
                            <%--<%}%>--%>
                    </div>
                </div>
            </c:forEach>
        </div>


    </div>
</div>

<script>

    function addCommentBox() {

        console.log("fired");

        var commentBox = document.createElement("textarea");
// commentBox.type = "text";
        commentBox.id = "comment-box";
        commentBox.classList.add("form-control", "col-md-12");
        commentBox.placeholder = "Write your comment!";
        commentBox.name = "comment";

        var target = document.getElementById("comment-div");
        target.append(commentBox);

        var commentButton = document.getElementById("comment-button");
        commentButton.disabled = true;

        var cancelButton = document.createElement("button");
        cancelButton.id = "cancel-button-btn";
        cancelButton.classList.add("btn", "btn-warning");
        cancelButton.addEventListener("click", removeCommentBox, true);
        cancelButton.innerHTML = "Cancel";

        var cancelDiv = document.getElementById("cancel-button");
        cancelDiv.append(cancelButton);

        var submitButton = document.createElement("input");
        submitButton.id = "submit-button-btn";
        submitButton.classList.add("btn", "btn-warning");
        submitButton.type = "submit";
        submitButton.innerHTML = "Submit";

        var submitDiv = document.getElementById("submit-button");
        submitDiv.append(submitButton);
        console.log(submitButton);

    }

    function removeCommentBox() {

        var commentBox = document.getElementById("comment-box");
        var submitButton = document.getElementById("submit-button-btn");
        var cancelButton = document.getElementById("cancel-button-btn");
        var commentButton = document.getElementById("comment-button");
        commentBox.remove();
        submitButton.remove();
        cancelButton.disabled = true;
        cancelButton.remove();
        commentButton.disabled = false;

    }

    function modifyArticleButton() {

        var modifyButton = document.getElementById("modify-button");
        modifyButton.disabled = true;

        var titleBox = document.createElement("input");
        titleBox.type = "text";
        titleBox.name = "titleChange";
        titleBox.value = document.getElementById("article-title").innerText;

        var contentBox = document.createElement("textarea");
        contentBox.id = "change-content";
        contentBox.name = "contentChange";
        contentBox.style.cssText = 'width:100%';
        contentBox.value = document.getElementById("article-content").innerText;

        var submitChanges = document.createElement("button");
        submitChanges.style.cssText = 'margin-top:10px';
        submitChanges.classList.add("btn", "btn-success");
        submitChanges.type = "submit";
        submitChanges.innerHTML = "Submit Changes";

        var target1 = document.getElementById("article-title-div");
        target1.append(titleBox);
        var remove1 = document.getElementById("article-title");
        remove1.remove();

        var target2 = document.getElementById("article-content-div");
        target2.append(contentBox);
        var remove2 = document.getElementById("article-content");
        remove2.remove();


        var buttonTarget = document.getElementById("submit-changes");
        buttonTarget.append(submitChanges);
    }

    function removeArticleModifiers() {
        // to do
    }
</script>


</body>
</html>