<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="DAO.AccountDAO" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>

<html>
<head>
    <title>Admin: REMOVE users.</title>
</head>
<body>

<%!
    AccountDAO accountDAO;
    String realPath;
    int counter = 0;
    List<Integer> listToDelete = new ArrayList<>();
    List<HashMap<String, String>> usersList;
    boolean status_completed = false;
%>

<%

    if (session.getAttribute("accountRole") != null) {


        String role = (String) session.getAttribute("accountRole");
//    String login = (String) session.getAttribute("login");

        System.err.println("role type is: " + role);

        if (!role.equals("1")) {
            System.out.println("Not admin rights for user while accessing admin page, redirection to index.jsp");
            response.sendRedirect("/index.jsp");
        } else {


%>

<span style="font-size: 30px">ADMIN page: REMOVE users.</span>
<% if (counter >= 2) {
%> <span style="color: red; font-size: 30px"> &nbsp; &nbsp; STATUS: DELETED!</span><%
        counter = 0;
    }%>
<br><br>


<%
    System.err.println("   ***   Admin module: remove users");
    realPath = application.getRealPath(application.getInitParameter("sql_config"));
    accountDAO = new AccountDAO(realPath);
    //read data only on the first call of this page
    usersList = accountDAO.getAllUsersByRoleID(2);
    System.err.println("amount of users: " + usersList.size());
    System.err.println("fetched from database: ");


    //   ***   if selected -> just delete, not null
    for (int i = 0; i < usersList.size(); i++) {
        String valueName = "user" + i;
        String value = "" + request.getParameter(valueName);
        if (value.equals("true")) {
            int id = Integer.parseInt(usersList.get(i).get("id"));
            System.err.println("ID to delete:" + id);
            int rows = accountDAO.deleteAccount(id);
            if (rows != 0)
                status_completed = true;
        }
    }

    counter++;
    if (status_completed) {
//        counter = 0;

        status_completed = false;
        System.err.println("DONE 'DELETE USERS'");
//        System.err.println("counter=" + counter);
        response.setHeader("REFRESH", "0");

        //   ***   REDIRECT TO ITSELF FOR REFRESHING THE PAGE   ***
//        request.getRequestDispatcher("/admin_remove_users.jsp").forward(request, response);
//        response.sendRedirect("/admin_remove_users.jsp");

//        RequestDispatcher rd = request.getRequestDispatcher("")
    }
%>

<form action="/admin_remove_users.jsp" method="post">
    <button type="submit" name="remove">REMOVE USERS</button>
    &nbsp; &nbsp;
    <button type="button" onclick="location.href='/index.jsp'">MAIN PAGE</button>
    <br><br>
    <table border="2" style="width: 50%">
        <tr style=" background: #C3C7C9">
            <th>ID</th>
            <th>Login</th>
            <th>Email</th>
            <th>Last Name</th>
            <th>SELECT</th>
        </tr>
        <%
            for (int i = 0; i < usersList.size(); i++) {
        %>
        <tr>
            <td><strong><%=usersList.get(i).get("id")%>
            </strong>
            </td>
            <td><%=usersList.get(i).get("login")%>
            </td>
            <td><%=usersList.get(i).get("email")%>
            </td>
            <td><%=usersList.get(i).get("lastname")%>
            </td>
            <td><input type="checkbox" value="true" name="user<%=i%>"></td>
        </tr>
        <%
            }
        %>

    </table>

    <br><br>
    <%--<button type="submit" name="remove">REMOVE USERS</button>--%>
</form>

<br><br>
<%--<a href="index.jsp">MAIN PAGE</a>--%>
</body>
</html>


<%

        }


    } else {
        System.err.println("no one logged in, session is null");
        response.sendRedirect("/index.jsp");
    }

%>

