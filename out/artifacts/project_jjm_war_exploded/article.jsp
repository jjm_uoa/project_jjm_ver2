<%--
  Created by IntelliJ IDEA.
  User: jesseskralskis
  Date: 5/21/18
  Time: 3:53 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <%@include file="style/style_head.jsp" %>
    <link rel="stylesheet" href="style/css/main-style.css" type="text/css">
    <title>Add Article</title>
</head>
<body>
<%@include file="navbar.jsp" %>
<div class="container-fluid" id="add-art-page">
    <div class="container-fluid" id="add-art-float">
        <div>

            <form action="/link" method="get">
                <fieldset>
                    <legend class="legend"><strong>Add an Article.</strong></legend>
                    <hr>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="control-label" for="title">Article title:</label>
                            <input type="text" name="title" id="title"><br>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <label class="control-label" for="content">Content:</label>
                            <textarea id="content" name="content" cols="100"></textarea>
                        </div>
                    </div>


                    <div class="row">
                        <div class="input col-md-12" style="padding-left: 15px">
                            <button class="btn btn-warning" type="submit" value="Submit">Submit your article&nbsp;
                                <i class="fas fa-paper-plane"></i>
                            </button>
                        </div>
                    </div>


                </fieldset>
            </form>
        </div>
    </div>
</div>
</body>
</html>

