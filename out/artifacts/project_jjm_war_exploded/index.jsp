<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@include file="style/style_head.jsp" %>
    <link rel="stylesheet" href="style/css/main-style.css" type="text/css">
    <link rel="stylesheet" href="style/js/main-script.js">
    <title>Welcome</title>
</head>

<body>
<div class="container-fluid" id="welcome-page">
    <div class="container-fluid" id="welcome-float">

        <h1 id="title" style="text-align: center"><strong>Technology.</strong></h1>
        <p>A blog from the future, for now.</p>

            <a href="signup.jsp" class="btn btn-warning">Sign Up&nbsp; <i class="fas fa-user-plus"></i></a>
            <a href="login.jsp" class="btn btn-warning">Login&nbsp; <i class="fas fa-sign-in-alt"></i></a>
        <form id="welcome-float-form" method="get" action="/Browse">
            <button id="browse-button" type="submit" href="articlePage.jsp" class="btn btn-danger" style="border-radius: 0; width: 120px; margin-top: 20px">Browse &nbsp; <i class="fas fa-arrow-circle-right"></i></button>
        </form>
        <div class="container" id="other-links">
            <a class="text-link"><strong>Contact.</strong></a>
            <a class="text-link"><strong>About.</strong></a>
        </div>


    </div>
</div>

</body>
</html>
