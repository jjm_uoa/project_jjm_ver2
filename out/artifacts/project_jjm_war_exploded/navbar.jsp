<%--
  Created by IntelliJ IDEA.
  User: macbookpro
  Date: 22/05/18
  Time: 1:24 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<nav class="navbar navbar-expand-sm bg-dark navbar-dark fixed-top" style="background-color: black">
    <a onclick="openNav()" class="navbar-brand" href="#"><i class="fas fa-bars"></i> &nbsp; Navigate</a>
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" id="navbar-header-text" href="#" style="color: #ffc107"><strong>Technology.</strong></a>
        </li>
    </ul>


    <ul class="navbar-nav ml-auto">
        <% if (session.getAttribute("id") == null) {%>
        <a class="btn btn-success" href="/login.jsp" style="border-radius: 0">Login</a>
        <%}%>
        <% if (session.getAttribute("id") != null) {%>
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
               aria-haspopup="true" aria-expanded="false">
                User Options
            </a>


            <%-- user dropdown
            able to add function to the below form elements - style can come later - contained here are logout and
            'my articles' which will display all the articles from the user--%>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown" style="float: right">


                <%--<form action="" method="">--%>
                    <%--<button type="submit" class="btn btn-danger dropdown-item" href="#">My Articles</button>--%>
                    <%--<div class="dropdown-divider"></div>--%>
                    <%--<button class="btn btn-danger dropdown-item" href="#">Logout</button>--%>
                <%--</form>--%>

                    <button class="btn btn-danger dropdown-item" type="button" onclick="location.href='/updateUserInfo.jsp'">Update User Details</button>
                    <button class="btn btn-danger dropdown-item" type="button" onclick="location.href='/deleteAccount.jsp'">Delete Account</button>
                    <button class="btn btn-danger dropdown-item" type="button" onclick="location.href='/logout.jsp'">Logout</button>


            </div>
        </li>
        <%}%>

        <% if ((session.getAttribute("id") != null) && (session.getAttribute("accountRole") != null)) {
//            System.err.println("ACCOUNT ROLE:" + session.getAttribute("accountRole"));
            if (session.getAttribute("accountRole").equals("1")) {

        %>
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
               aria-haspopup="true" aria-expanded="false">
                Admin Features
            </a>


            <%-- admin dropdown --%>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown" style="float: right">

                <%--<form action="/admin_add_users.jsp" method="post">--%>
                <%--<button type="submit" class="btn btn-danger dropdown-item" href="#">Add users</button>--%>
                <%--<div class="dropdown-divider"></div>--%>
                <%--</form>--%>

                    <button class="btn btn-danger dropdown-item" type="button" onclick="location.href='/admin_add_users.jsp'">Add users</button>
                    <button class="btn btn-danger dropdown-item" type="button" onclick="location.href='/admin_remove_users.jsp'">Delete users</button>
                    <button class="btn btn-danger dropdown-item" type="button" onclick="location.href='/admin_hide_articles.jsp'">Hide\Show articles\comments</button>

                    <button class="btn btn-danger dropdown-item" type="button" onclick="location.href='/logout.jsp'">Logout</button>


                    <%--<button class="btn btn-danger dropdown-item" href="#">Logout</button>--%>


            </div>
        </li>
        <%
                }
            }
        %>
    </ul>
</nav>


<div id="mySidenav" class="sidenav">
    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
    <a href="index.jsp">Welcome</a>

    <!-- must surround with a form for redirect -->
    <a href="articlePage.jsp">Articles</a>


    <a href="signup.jsp">SignUp</a>
    <a href="login.jsp">Login</a>
    <a href="article.jsp">Add Article</a>
    <%
        if (session.getAttribute("id") != null) {
    %><a href="updateUserInfo.jsp">Update info</a><%
    }
%>
</div>

<script>
    function openNav() {
        document.getElementById("mySidenav").style.width = "250px";
    }

    function closeNav() {
        document.getElementById("mySidenav").style.width = "0";
    }
</script>
