<%@ page import="java.util.List" %>
<%@ page import="DAO.Models.Article" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: jesseskralskis
  Date: 5/21/18
  Time: 4:15 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <%@include file="style/style_head.jsp" %>
    <link rel="stylesheet" href="style/css/main-style.css" type="text/css">
    <script language="JavaScript" href="style/js/main-script.js"></script>
    <%--<link rel="stylesheet" href="style/js/main-script.js">--%>
    <title>Browse</title>
</head>
<body>
<%@include file="navbar.jsp" %>


<div class="container-fluid" id="article-page">
    <div class="container-fluid" id="article-float">

        <p class="header-text"><strong>Articles.</strong></p>
        <p id="header-caption"><i>Here you will find articles on the subject of new technology. Read something existing,
            or add your own.</i></p>
        <hr>

        <c:if test="${sessionScope.login != null}">
            <div class="row">
                <div class="col-md-12" id="add-art-button">
                    <form action="/postarticles" method="get">
                        <button class="btn btn-danger" type="submit" value="Submit"
                                style="border-radius: 0; margin: 20px; width: 300px">Add an Article &nbsp;<i
                                class="fas fa-pencil-alt"></i>
                        </button>
                    </form>
                </div>
            </div>
        </c:if>


        <form action="" method="">
            <div class="topnav">
                <input type="text" placeholder="Search..">
                <button type="submit">search</button>
            </div>
        </form>


        <div class="row">
            <c:forEach var="user" items="${articles}">
                <div class="card col-md-6 col-xs-12">
                    <div class="card-body">
                        <h4 class="card-title">${user.title}</h4>
                        <hr>
                        <p class="card-text"> ${user.content}
                        </p>
                        <p class="user-info">${user.fname}&nbsp;${user.lname}&nbsp;//&nbsp;${user.time}</p>
                        <form name="frm" action="/articleView" method="get">
                            <input type="hidden" name="thisArticleID" value=${user.articleID}>
                            <button name="btn" type="submit" class="btn btn-warning" style="border-radius: 0">Read More
                                &nbsp; <i class="fas fa-book"></i></button>
                        </form>
                    </div>


                </div>
            </c:forEach>

        </div>
    </div>
</div>

</body>
</html>


</body>
</html>
