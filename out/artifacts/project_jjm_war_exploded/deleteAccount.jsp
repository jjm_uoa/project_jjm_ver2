<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="DAO.AccountDAO" %>

<html>
<head>
    <title>Delete Account</title>
</head>
<body>

<br><br>


<%!
    int rows_affected = 0;
%>

<%--we need id + confirmation of user password, then we can delete an account--%>
<%--first call Login page and get validation--%>
<%
    System.err.println("   ***   Erase user module   ***");
    String sessionID = (String) session.getAttribute("id");

    if (sessionID != null) {

        session.setAttribute("erase_mode", "true");

        String attempt_to_delete_account = (String) session.getAttribute("attempt_to_delete_account");

        String realPath = application.getRealPath(application.getInitParameter("sql_config"));
        AccountDAO accountDAO = new AccountDAO(realPath);

        String confirm_to_erase = (String) session.getAttribute("confirm_to_erase");
        System.out.println("confirm_to_erase: " + confirm_to_erase);

        // *** will receive a confirmation from Login servlet, first run == "false"
        if (confirm_to_erase == "true") {
            session.getServletContext();
            System.err.println("session ID:" + session.getAttribute("id"));
            int id = Integer.valueOf(session.getAttribute("id").toString());
            System.err.println("starting deleting an account with id: " + id);
            System.err.println("id to delete: " + id);
            rows_affected = accountDAO.deleteAccount(id);
            session.setAttribute("erase_mode", "false"); //reset mode
            session.setAttribute("attempt_to_delete_account", null);

            session.removeAttribute("id");
%>
<%
    }
%>

<h2>
    Delete Account form.
    &nbsp; &nbsp;
    <%
        if (rows_affected == 3) { %> <span style="color: red">Account deleted successfully!</span> <%
} else {
    attempt_to_delete_account = (String) session.getAttribute("attempt_to_delete_account");
    if ((rows_affected == 0) && (attempt_to_delete_account != null)) {
        session.setAttribute("erase_mode", "false"); //reset mode
        session.setAttribute("attempt_to_delete_account", null);

%><span style="color: red">Error deleting account!</span><%
        }
    }
%>
</h2>
<form action="/Login" method="post">

    <%
        if (rows_affected == 3) {
            rows_affected = 0;
            session.invalidate();  // remove session, next refresh will be rejected, now just disable button DELETE
//            session.setAttribute("erase_mode", "false"); //preventing error if delete user and refresh page
    %>
    <button type="submit" disabled>DELETE</button>
    <%} else {%>
    <button type="submit">DELETE</button>
    <%
        }
    %>


    &nbsp; &nbsp;
    <button type="button" onclick="location.href='/index.jsp'">MAIN PAGE</button>
    <br><br>
    <input type="text" name="username"><br><br>
    <input type="password" name="password"><br><br>
    <br>
</form>

<%
    } else {
        System.err.println("User must be logged in before accessing deleteAccount.jsp, redirecting to index.jsp!");
        response.sendRedirect("/index.jsp");
    }
%>
</body>
</html>
