<%@ page import="java.util.List" %>
<%@ page import="DAO.Models.Article" %>

<%--
  Created by IntelliJ IDEA.
  User: jesseskralskis
  Date: 5/21/18
  Time: 4:15 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <%@include file="style/style_head.jsp" %>
    <link rel="stylesheet" href="style/css/main-style.css" type="text/css">
    <script language="JavaScript" href="style/js/main-script.js"></script>
    <title>Browse</title>
</head>
<body>
<div>
    <%@include file="navbar.jsp" %>


    <div class="container-fluid" id="article-page">
        <div class="container-fluid" id="article-float">

            <p class="header-text"><strong>Articles.</strong></p>
            <p><strong>${titleMessage}</strong></p>
            <p id="header-caption"><i>Here's some articles about Technology, don't see anything you like? Add your own
                below!</i></p>
            <hr>


            <div class="row">
                <div class="col-md-6">
                    <form action="/search" method="get">

                        <div class="topnav">
                            <div class="col-md-12">
                                <input type="text" name="searchBox" placeholder="Search..">
                            </div>
                            <div class="input col-md-6">
                                <select id="search-select" class="form-control" name="searchCriteria"
                                        style="margin-top: 10px; margin-left: 15px; border-radius: 0">
                                    <option>Criteria...</option>
                                    <option value="title" name="searchTitle" type="hidden">Article Title</option>
                                    <option value="user_id">Username</option>
                                    <option value="created_time">Date</option>
                                </select>
                            </div>
                            <button class="btn btn-primary" type="submit" style="margin-left: 20px; margin-top: 5px">
                                search
                            </button>
                        </div>

                    </form>
                </div>
            </div>


            <hr>
            <c:if test="${sessionScope.login != null}">
                <div class="row">
                    <div class="col-md-12" id="add-art-button">
                        <form action="/postarticles" method="get">
                            <button class="btn btn-danger" type="submit" value="Submit"
                                    style="border-radius: 0; margin: 20px; width: 300px">Add an Article &nbsp;<i
                                    class="fas fa-pencil-alt"></i>
                            </button>
                        </form>
                    </div>
                </div>
            </c:if>


            <div class="row">

                <c:forEach var="user" items="${articles}">
                    <div class="card col-md-6 col-xs-12">
                        <div class="card-body">
                            <h4 class="card-title">${user.title}</h4>
                            <hr>
                            <div id="card-text">
                                <p class="card-text"> ${user.content}
                                </p>
                            </div>
                            <hr>

                            <p class="user-info">${user.fname}&nbsp;${user.lname}&nbsp;//&nbsp;${user.time}</p>
                            <form name="frm" action="/articleView" method="get">
                                <input type="hidden" name="thisArticleID" value=${user.articleID}>
                                <button name="btn" type="submit" class="btn btn-warning" style="border-radius: 0">Read
                                    More
                                    &nbsp; <i class="fas fa-book"></i></button>
                            </form>
                        </div>


                    </div>
                </c:forEach>

            </div>
        </div>
    </div>
</div>

</body>
</html>


