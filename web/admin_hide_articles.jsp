<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="DAO.Article_betaDAO" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<html>
<head>
    <title>Admin: Hide articles.</title>
</head>
<body>

<%!
    int counter = 0;
%>

<%
    if (session.getAttribute("accountRole") != null) {

        String role = (String) session.getAttribute("accountRole");
//    String login = (String) session.getAttribute("login");

        System.err.println("role type is: " + role);

        if (!role.equals("1")) {
            System.out.println("Not admin rights for user while accessing admin page, redirection to index.jsp");
            response.sendRedirect("/index.jsp");
        } else {


            String realPath = application.getRealPath(application.getInitParameter("sql_config"));
            Article_betaDAO article_betaDAO = new Article_betaDAO(realPath);
            List<HashMap> articlesList = article_betaDAO.getAllArticles();
            List<HashMap> commentsList = article_betaDAO.getAllComments();
            System.err.println("num of articles = " + articlesList.size());
            System.err.println("num of comments = " + commentsList.size());
%>

<%

    List<Integer> list_int = new ArrayList<>();
    list_int.clear();
    System.err.print("status to HIDE for the following ID --articles-- : ");
    for (int i = 0; i < articlesList.size(); i++) {
        String art_id_string = request.getParameter("art_chkbox" + i);
        if (art_id_string != null) {
            int art_id = Integer.parseInt(art_id_string);
            list_int.add(art_id);
        }
    }
    for (int i : list_int)
        System.err.print(i + ", ");
    System.err.println();

    article_betaDAO.updateArticlesStatus(list_int);

    list_int.clear();
    System.err.print("status to HIDE for the following ID --comments-- : ");
    for (int i = 0; i < commentsList.size(); i++) {
        String comment_chkbox_string = request.getParameter("comment_chkbox" + i);
        if (comment_chkbox_string != null) {
            int comment_chkbox = Integer.parseInt(comment_chkbox_string);
            list_int.add(comment_chkbox);
        }
    }
    for (int i : list_int)
        System.err.print(i + ", ");
    System.err.println();

    article_betaDAO.updateArticlesComments(list_int);
    list_int.clear();

    counter++;



%>

<span style="font-size: 30px">Admin: HIDE \ SHOW  articles & comments.</span>
<%
    if (counter >= 2) {
        articlesList = article_betaDAO.getAllArticles();
        commentsList = article_betaDAO.getAllComments();

%>
<span style="color: red; font-size: 30px"> &nbsp; &nbsp; STATUS: COMPLETED!</span
<%
        //        response.sendRedirect("/admin_hide_articles.jsp");
        counter = 0;
    }
%>

<br><br>
<form action="/admin_hide_articles.jsp" method="post">
    <button type="submit" name="hide">HIDE</button>
    &nbsp; &nbsp;
    <button type="button" onclick="location.href='/index.jsp'">MAIN PAGE</button>
    <%--&nbsp; &nbsp;--%>
    <%--<button type="button" onclick="location.href='/admin_hide_articles.jsp'">REFRESH</button>--%>
    <br><br>

    <table border="2">
        <tr style=" background: #C3C7C9">
            <th>Title</th>
            <th>Content</th>
            <th>Login</th>
            <th>Status</th>
        </tr>
        <%
            for (int i = 0; i < articlesList.size(); i++) {
        %>
        <tr>
            <td><strong><%=articlesList.get(i).get("title")%>
            </strong>
            </td>
            <td>
                <%=articlesList.get(i).get("content")%>

                <%--if no comment we dont want to print a table inside a table--%>
                <%
                    String resultLng = "";
                    String art_id_chk = (String) articlesList.get(i).get("art_id");
                    for (int j = 0; j < commentsList.size(); j++) {
                        if (art_id_chk.equals(commentsList.get(j).get("art_id"))) {
                            resultLng += commentsList.get(j).get("content");
                        }
                    }
                    if (!resultLng.equals("")) {
                %>

                <%--***   FORM INSIDE FORM   ***--%>
                <form action="/admin_hide_articles.jsp" method="post"></form>
                <%--<button type="submit" name="hide">HIDE</button>--%>
                <table border="2">
                    <tr style=" background: #C3C7C9">
                        <th>Comment</th>
                        <th> &nbsp; &nbsp; User &nbsp; &nbsp;</th>
                        <th>
                            <button type="submit" name="hide">&nbsp; &nbsp;SELECT&nbsp; &nbsp;</button>
                        </th>
                    </tr>
                    <tr>
                        <%
                            String art_id = (String) articlesList.get(i).get("art_id");
                            for (int j = 0; j < commentsList.size(); j++) {

                                if (art_id.equals(commentsList.get(j).get("art_id"))) {
                        %>

                        <td>
                            <%=commentsList.get(j).get("content")%>
                        </td>

                        <td>
                            <%=commentsList.get(j).get("login")%>
                        </td>

                        <td>
                            <%
                                String status = (String) commentsList.get(j).get("hidden");
                                if (status == null) status = " ";
                                else if (status.equals("1")) status = " checked";
                                else status = " ";
                            %>
                            <input type="checkbox" value="<%=commentsList.get(j).get("comment_id")%>"
                                   name="comment_chkbox<%=j%>" <%=status%>>
                        </td>

                    </tr>
                    <%
                            }
                        }
                    %>
                </table>
                <%--***   FORM INSIDE FORM   ***--%>

                <%}%>


            </td>
            <td><%=articlesList.get(i).get("login")%>
            </td>

            <td>
                <%
                    String status = (String) articlesList.get(i).get("hidden");
                    if (status == null) status = " ";
                    else if (status.equals("1")) status = " checked";
                    else status = " ";
                %>
                <input type="checkbox" value="<%=articlesList.get(i).get("art_id")%>"
                       name="art_chkbox<%=i%>" <%=status%>>
            </td>

        </tr>
        <%
            }
        %>
    </table>
</form>

<br><br>-------<br><br>

<%
        }
    } else {
        System.err.println("no one logged in, session is null");
        response.sendRedirect("/index.jsp");
    }
%>
</body>
</html>
