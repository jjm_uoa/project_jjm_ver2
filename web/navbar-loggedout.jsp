<%--
  Created by IntelliJ IDEA.
  User: macbookpro
  Date: 22/05/18
  Time: 1:24 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<nav class="navbar navbar-expand-sm bg-dark navbar-dark fixed-top" style="background-color: black">
    <a onclick="openNav()" class="navbar-brand" href="#"><i class="fas fa-bars"></i> &nbsp; Navigate</a>
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" id="navbar-header-text" href="#" style="color: #ffc107"><strong>Technology.</strong></a>
        </li>
    </ul>

    <%-- user dropdown
                able to add function to the below form elements - style can come later - contained here are logout and
                'my articles' which will display all the articles from the user--%>
    <ul class="navbar-nav ml-auto">
        <% if (session.getAttribute("id") != null) {%>
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown"
               aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-user"></i>&nbsp;
            </a>


            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown" style="float: right">

                <form action="/Browse" method="get">
                    <button class="btn btn-danger dropdown-item" type="button"
                            onclick="location.href='/updateUserInfo.jsp'">Update User Details
                    </button>
                    <button class="btn btn-danger dropdown-item" type="submit" onclick="location.href='/articlePage'">My
                        Articles
                    </button>
                    <button class="btn btn-danger dropdown-item" type="button"
                            onclick="location.href='/deleteAccount.jsp'">Delete Account
                    </button>
                    <button class="btn btn-danger dropdown-item" type="button" onclick="location.href='/logout.jsp'">
                        Logout
                    </button>
                </form>

            </div>
        </li>
        <%}%>

        <% if ((session.getAttribute("id") != null) && (session.getAttribute("accountRole") != null)) {
//            System.err.println("ACCOUNT ROLE:" + session.getAttribute("accountRole"));
            if (session.getAttribute("accountRole").equals("1")) {

        %>

        <%-- admin dropdown --%>
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
               aria-haspopup="true" aria-expanded="false">
                Admin Features
            </a>


            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown" style="float: right">

                <button class="btn btn-danger dropdown-item" type="button"
                        onclick="location.href='/admin_add_users.jsp'">Add users
                </button>
                <button class="btn btn-danger dropdown-item" type="button"
                        onclick="location.href='/admin_remove_users.jsp'">Delete users
                </button>
                <button class="btn btn-danger dropdown-item" type="button"
                        onclick="location.href='/admin_hide_articles.jsp'">Hide\Show articles\comments
                </button>

                <button class="btn btn-danger dropdown-item" type="button" onclick="location.href='/logout.jsp'">
                    Logout
                </button>


                <%--<button class="btn btn-danger dropdown-item" href="#">Logout</button>--%>


            </div>
        </li>
        <%
                }
            }
        %>
    </ul>
</nav>


<div id="mySidenav" class="sidenav" style="background-color: rgba(255, 255, 255, 0.9)">

    <button class="btn btn-warning closebtn" onclick="closeNav()"
            style="width: 100%; text-align: center; background-color: transparent; border: 0"><i
            class="fas fa-angle-double-left"></i></button>
    <button class="btn btn-warning" onclick="location.href='index.jsp'"
            style="width: 100%; text-align: center;background-color: transparent; border: 0">Home
    </button>

    <!-- must surround with a form for redirect -->
    <form action="/Browse" method="get" id="sidebar-form" style="margin: 0">
        <button class="btn btn-warning" type="submit" href="articlePage.jsp"
                style="width: 100%; text-align: center;background-color: transparent; border: 0">
            Articles
        </button>
    </form>


    <button class="btn btn-warning" onclick="location.href='signup.jsp'"
            style="width: 100%; text-align: center; background-color: transparent; border: 0">Sign Up
    </button>
    <button class="btn btn-warning" onclick="location.href='login.jsp'"
            style="width: 100%; text-align: center; background-color: transparent; border: 0">Login
    </button>
    <%
        if (session.getAttribute("id") != null) {
    %>
    <button class="btn btn-warning" onclick="location.href='article.jsp'"
            style="width: 100%; text-align: center; background-color: transparent; border: 0">Add Article
    </button>

    <button class="btn btn-warning" onclick="location.href='updateUserInfo.jsp'"
            style="width: 100%; text-align: center; background-color: transparent; border: 0">Update Info
    </button>
    <%
        }
    %>


</div>


<script>
    function openNav() {
        document.getElementById("mySidenav").style.width = "250px";
    }

    function closeNav() {
        document.getElementById("mySidenav").style.width = "0";
    }
</script>
