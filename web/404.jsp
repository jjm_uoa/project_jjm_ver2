<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@include file="style/style_head.jsp" %>
    <link rel="stylesheet" href="style/css/main-style.css" type="text/css">
    <link rel="stylesheet" href="style/js/main-script.js">
    <title>404. Oops</title>
</head>

<body>
<div>
    <div class="container-fluid" id="notfound-page">
        <div class="container-fluid" id="notfound-page-float">

            <h1 style="text-align: center"><strong>404.</strong></h1>
            <p>This page hasn't been invented yet. Sorry about that.</p>
            <br>
            <br>

            <p>Where would you like to go?</p>
            <a href="index.jsp" class="btn btn-warning">Home&nbsp; <i class="fas fa-home"></i></a>

            <form id="welcome-float-form" method="get" action="/Browse">
                <button id="browse-button" type="submit" href="articlePage.jsp" class="btn btn-danger" style="border-radius: 0; width: 120px; margin-top: 20px">Browse &nbsp; <i class="fas fa-arrow-circle-right"></i></button>
            </form>


        </div>
    </div>
    <%@include file="footer.jsp"%>
</div>

</body>
</html>
