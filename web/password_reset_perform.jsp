<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="DAO.PasswordDAO" %>
<%@ page import="Utils.Passwords" %>
<%@ page import="java.util.Random" %>
<%@ page import="static Utils.Passwords.getNextSalt" %>
<%@ page import="static Utils.Passwords.hash" %>
<%@ page import="static Utils.Passwords.base64Encode" %>
<%@ page import="DAO.AccountDAO" %>
<html>
<head>
    <title>Reset Password Perform</title>
</head>
<body>


<%!
    //    Passwords passwords = new Passwords();
//    PasswordDAO passwordDAO = new PasswordDAO();
//    AccountDAO accountDAO;
    Random random = new Random();
    //    int counter = 0;
%>

<%
    ServletContext context = pageContext.getServletContext();
    String realPath = application.getRealPath(application.getInitParameter("sql_config"));
    PasswordDAO passwordDAO = new PasswordDAO(realPath);
    AccountDAO accountDAO = new AccountDAO(realPath);

    System.err.println("   ***   password_reset_perform.jsp module   ***");
    String login = "" + session.getAttribute("login_psw_reset");
    System.err.println("   ^   login to reset: " + login);

    if ((session.getAttribute("login_psw_reset") != null) &&
            (!(session.getAttribute("login_psw_reset").equals("")))) {

%>

<br>
<h2>Password RESET form</h2>

<form action="./password_reset_perform.jsp" method="post">
    <button type="submit" name="submit">RESET</button>&nbsp; &nbsp;
    <button type="button" onclick="location.href='/index.jsp'">MAIN PAGE</button>
    <br><br>
    <input type="password" name="pas1" placeholder="password">
    <br><br>
    <input type="password" name="pas2" placeholder="confirm password">
    <br><br>
</form>
<%


        //        System.err.println("   ^   login to reset: " + login);

        String password1 = "" + request.getParameter("pas1");
        String password2 = "" + request.getParameter("pas2");

        String logMessage = "";

        System.err.println("pas1:" + password1);
        System.err.println("pas2:" + password2);


        //   ***   pass1 == pass2 and they are not empty
        if ((password1.equals(password2)) &&
                !(password1.equals("")) &&
                !(password2.equals("")) &&
                ((request.getParameter("pas1")) != null) &&
                ((request.getParameter("pas2")) != null)) {
            System.err.println("   ~   passwords are matches & not empty");

            //additional logic to check input fields
            String ps = password1;

            //check password strength [4 at least, upper and lower case, 1 number\or special character]
            if (ps.length() < 4) logMessage += "Your password is less than 4 characters\n";
            if (ps.equals(ps.toLowerCase())) logMessage += "Your password must contain at least 1 upper character\n";
            if (ps.equals(ps.toUpperCase())) logMessage += "Your password must contain at least 1 lower character\n";
            ps = ps.toLowerCase();
            String remover = "abcdefghijklmnopqrstuvwxyz";
            for (int i = 0; i < remover.length(); i++) {
                ps = ps.replace("" + remover.charAt(i), "");
            }
            if (ps.length() == 0) logMessage += "Your password must contain at least 1 number or special character\n";


            System.err.println("log: " + logMessage);

            if (logMessage.equals("")) {
                System.err.println("proceed with password update");

                // ***  some code to update password info
                // need clean login_psw_reset after
//                session.removeAttribute("login_psw_reset");

                int iterations = random.nextInt(33) + 32;
                byte[] salt = getNextSalt(iterations);
                String password = password1;
                char[] charArray = password.toCharArray();
                byte[] hashedPassword = hash(charArray, salt, iterations);
                String passwordHashedString = base64Encode(hashedPassword);

                System.err.println("salt length: " + salt.length);
                System.err.println("not processed password: " + password);
                System.err.println("hashed password string: " + passwordHashedString);


                //   ***   updates passwords in database
                //   ***   1. update HASH
                passwordDAO.updatePasswordHash(login, passwordHashedString);
                //   ***   2. get ID by Login
                int id = passwordDAO.getIDbyLogin(login);
                System.err.println("ID: " + id);
                //   ***   3. update salt 7 iterations in password_specs
                passwordDAO.pushData(id, salt, iterations);
                System.err.println("password updated!");
            } else {
                System.err.println("errors while entering a password are: " + logMessage);
            }


//        counter = -1;
        } else {
            System.err.println("   ~   passwords are different or (and) empty");
//        counter = -1;


        }

    } else {
        System.err.println("no valid login in session to reset has been provided");
        response.sendRedirect("./index.jsp");
    }

//    counter++;


%>

</body>
</html>
