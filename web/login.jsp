<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@include file="style/style_head.jsp" %>
    <link rel="stylesheet" href="style/css/main-style.css" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"/>
    <title>Login</title>
</head>
<body>

<%
    session.setAttribute("erase_mode", "false");

    String login_status = (String) session.getAttribute("login_status");
    System.err.println("login_status: " + login_status);
    if (login_status == null)
        login_status = "null";


%>

<% if (session.getAttribute("id") == null){%>
<%@include file="navbar-loggedout.jsp"%>
<%} else {%>
<%@include file="navbar.jsp" %>
<%}%>
<div class="container-fluid" id="login-page">
    <div class="container-fluid" id="login-float">


        <div>
            <form class="form-horizontal" action="/Login" method="post">
                <fieldset>
                    <legend class="legend"><strong>Login.</strong></legend>
                    <hr>
                    <div class="row">

                        <div class="form-group col-md-12">
                            <label class="control-label" for="username">Username</label>
                            <div class="input col-md-12">
                                <% if (login_status == "noSuchUser") {%>

                                <input type="text" id="username"
                                       placeholder="&#xf05e;&nbsp;Incorrect Username or Password"
                                       style="font-family: Arial, FontAwesome" name="username">
                                <%} else {%>


                                <input type="text" id="username" placeholder="&#xF007;"
                                       style="font-family: Arial, FontAwesome" name="username">
                                <%}%>
                            </div>
                        </div>

                        <div class="form-group col-md-12">
                            <label class="control-label" for="pass1">Password</label>
                            <div class="input col-md-12">

                                <% if (login_status == "wrongPassword") {%>
                                <input type="password" class="form-control" id="pass1"
                                       placeholder="&#xf05e;&nbsp;Incorrect Password"
                                       style="font-family: Arial, FontAwesome"
                                       name="password">
                                <%} else {%>
                                <input type="password" class="form-control" id="pass1" placeholder="&#xf084;"
                                       style="font-family: Arial, FontAwesome"
                                       name="password">
                                <%}%>

                            </div>
                        </div>

                        <div class="form-group col-md-12">
                            <div class="input col-md-12">
                                <button type="submit" id="submit" name="password" class="btn btn-warning">Login</button>
                            </div>
                        </div>

                        <div id="signup-button">

                            <p>Don't have an account?</p>
                            <button class="btn btn-warning" onclick="location.href='signup.jsp'">Sign Up</button>
                        </div>

                    </div>
                </fieldset>

            </form>
        </div>
    </div>
</div>

</body>
</html>
