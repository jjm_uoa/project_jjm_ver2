<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="DAO.AccountDAO" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.Map" %>
<%--<%@ page import="java.util.Enumeration" %>--%>

<html>
<head>
    <title>Update User Information</title>
</head>
<body>

<br><br>


<%!
    int counter = 0;
%>

<%
    String realPath = application.getRealPath(application.getInitParameter("sql_config"));
    AccountDAO accountDAO = new AccountDAO(realPath);

    int id;

    // *** THIS IS THE ONLY WORKING WAY TO GET A SESSION GLOBAL VALUES
    // *** COVERT FROM STRING TO INT EXACT THIS WAY!
    System.err.println("   ***   UPDATE USER INFO module   ***");
    String s = (String) session.getAttribute("id");
    if (s != null) {
        id = Integer.valueOf(s);

        //   ***   MOCK PARAMETER, DISABLE TWO UPPER LINES TO ACTIVATE   ***
//    id = 10;
        System.err.println("Processing user with an ID: " + id);

        // ** get all data by user ID to populate placeholders in form filed **
        HashMap<String, String> info = new HashMap<>();
        if (info.size() == 0)
            info = accountDAO.getInfoByID(id);

        System.err.println("  ~  read data from SQL to HASHMAP: ");
        for (Map.Entry<String, String> entry : info.entrySet()) {
            System.err.println("Key: " + entry.getKey() + ", Value: " + entry.getValue());
        }

        //   ***   READ DATA FROM ITSELF   ***
        String firstname = "" + request.getParameter("firstname");
        String lastname = "" + request.getParameter("lastname");
        String middlename = "" + request.getParameter("middlename");
        String about = "" + request.getParameter("about");
        String email = "" + request.getParameter("email");
        System.err.println("  ~  read data from form firstname: " + firstname +
                ", lastname: " + lastname + ", middlename: " + middlename +
                ", about: " + about + "read from form email: " + email +
                ", login in HASHMAP: " + info.get("login"));

        // IF FORM IS EMPTY, WILL USE DATA FROM DATABASE, FROM info HASHMAP
        if (firstname.length() == 0)
            firstname = info.get("firstname");

        if (lastname.length() == 0)
            lastname = info.get("lastname");

        if (middlename.length() == 0)
            middlename = info.get("middlename");

        if (about.length() == 0)
            about = info.get("about");

        if (email.length() == 0)
            email = info.get("email");

        System.err.println("counter:" + counter);
%>

<div>
    <h2>Update User Information
        <% if (counter == 1) {
            System.err.println("invoking updateAccount method");
            System.err.print("Parameters to submit# ");
            System.err.println("login: " + info.get("login") + ", firstname: " + firstname + ", lastname: " + lastname +
                    ", middlename: " + middlename + ", email: " + email + ", about: " + about);
            accountDAO.updateAccount(info.get("login"), firstname, lastname, middlename, email, about);
            counter = -1;
            info.clear();
            info = accountDAO.getInfoByID(id);
        %> <span style="color: red"> &nbsp; &nbsp; STATUS: COMPLETED!</span><%
            }
            counter++;
        %>
    </h2>

    <form action="/updateUserInfo.jsp" method="post">
        <%
            if (counter == 0) {%>
        <button type="submit" disabled>UPDATE</button>
        <%} else {%>
        <button type="submit">UPDATE</button>
        <%
            }
        %>
        &nbsp; &nbsp;
        <button type="button" onclick="location.href='/index.jsp'">MAIN PAGE</button>
        <br><br>
        <input type="text" name="firstname" placeholder="<%= info.get("firstname")%>"> firstname <br> <br>
        <input type="text" name="lastname" placeholder="<%= info.get("lastname")%>"> lastname <br> <br>
        <input type="text" name="middlename" placeholder="<%= info.get("middlename")%>"> middlename <br> <br>
        <input type="text" name="about" placeholder="<%= info.get("about")%>"> about <br> <br>
        <input type="text" name="email" placeholder="<%= info.get("email")%>"> email <br> <br>
    </form>

</div>

<%
    } else {
        System.err.println("accessing updateUserInfo.jsp without session.id parameter, redirection to index.jsp");
        response.sendRedirect("/index.jsp");
    }
%>

</body>
</html>