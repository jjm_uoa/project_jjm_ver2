<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: macbookpro
  Date: 23/05/18
  Time: 10:42 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@include file="style/style_head.jsp" %>

    <link rel="stylesheet" href="style/css/main-style.css" type="text/css">
    <script rel="script" href="style/js/comment-script.js"></script>

    <title>Article Title</title>

</head>
<body>

<%@include file="navbar.jsp"%>

<div class="container-fluid" id="article-view-page">
    <div class="container-fluid" id="article-view-float">
        <div class="row">
            <div class="card col-md-12 col-xs-12">
                <div class="card-body">


                    <%if (session.getAttribute("id") != null) {%>
                    <button onclick="modifyArticleButton()" class="btn btn-success">Modify Article</button>
                    <%}%>

                    <form action="" method="">
                        <h1 class="card-title" id="article-title">${article.title}</h1>

                        <%-- this is you button for submit changes, button appended to form --%>
                        <div id="submit-changes"></div>

                        <hr>
                        <p class="user-info">${article.fname} &nbsp;${article.lname},${article.time}</p>
                        <hr>
                        <p class="card-text" id="article-content">${article.content}</p>
                    </form>

                    <hr>

                    <%if (session.getAttribute("id") != null) {%>
                    <div class="row">
                        <div class="col-md-6">
                            <button id="comment-button" class="btn btn-warning" onclick="addCommentBox()">Comment on
                                this
                                article
                            </button>
                        </div>
                    </div>
                    <hr>
                    <%}%>
                </div>
            </div>


            <form action="/comments" method="get" class="form-horizontal">
                <fieldset>
                    <div class="row">
                        <div class="form-group col-md-12">
                            <div class="col-md-12" id="comment-div">
                                <!-- text box created here -->
                            </div>
                        </div>
                    </div>
                    <%--for testing--%>
                    <%--<input type="text" name="comment">--%>
                    <%--<input type="submit" class="btn btn-primary">--%>

                    <div class="row">
                        <div class="form-group col-md-6">
                            <form action="articleView.jsp" method="get">
                                <div id="submit-button"></div>
                            </form>
                        </div>
                        <div class="form-group col-md-6">
                            <div id="cancel-button"></div>
                        </div>
                    </div>
                </fieldset>
            </form>

            <p class="comment-header"><strong>Comments.</strong></p>

            <%--${comments}--%>
            <div class="row"></div>
            <c:forEach var="comment" items="${existComments}">
                <div class="col-md-12 comment-body">
                    <div id="comment-text">
                        <p id="comment">${comment.comment}</p>
                        <p id="comment-info">${comment.userId}&nbsp;//&nbsp;${comment.commentCreated}</p>
                        <%if (session.getAttribute("id") != null) {%>
                        <form action="" method="">
                            <button type="submit" class="btn btn-danger delete-comment">x</button>
                        </form>
                        <%}%>
                    </div>
                </div>
            </c:forEach>

            <%--<c:forEach var="comment" items="${existComments}">--%>
            <%--<tr>--%>
            <%--<p id="existComment">${comment.comment}</p>--%>

            <%--</tr>--%>

            <%--</c:forEach>--%>

        </div>


    </div>
</div>

<script>

    function addCommentBox() {

        console.log("fired");

        var commentBox = document.createElement("textarea");
// commentBox.type = "text";
        commentBox.id = "comment-box";
        commentBox.classList.add("form-control", "col-md-12");
        commentBox.placeholder = "Write your comment!";
        commentBox.name = "comment";

        var target = document.getElementById("comment-div");
        target.append(commentBox);

        var commentButton = document.getElementById("comment-button");
        commentButton.disabled = true;

        var cancelButton = document.createElement("button");
        cancelButton.classList.add("btn", "btn-warning");
        cancelButton.addEventListener("click", removeCommentBox, true);
        cancelButton.innerHTML = "Cancel";

        var cancelDiv = document.getElementById("cancel-button");
        cancelDiv.append(cancelButton);

        var submitButton = document.createElement("input");
        submitButton.classList.add("btn", "btn-warning");
        submitButton.type = "submit";
        submitButton.innerHTML = "Submit";

        var submitDiv = document.getElementById("submit-button");
        submitDiv.append(submitButton);
        console.log(submitButton);

    }

    function removeCommentBox() {

        var commentBox = document.getElementById("comment-box");
        document.remove(commentBox)
    }

    function modifyArticleButton() {
        var titleBox = document.createElement("textarea");
        titleBox.placeholder = document.getElementById("article-title").innerText;
        titleBox.name = "titleBox";

        var contentBox = document.createElement("textarea");
        contentBox.placeholder = document.getElementById("article-content").innerText;
        contentBox.name = "contentbox";

        var submitChanges = document.createElement("button");
        submitChanges.classList.add("btn", "btn-success");
        submitChanges.type = "submit";
        submitChanges.name = "submitChanges";
        submitChanges.innerHTML = "Submit Changes";

        var buttonTarget = document.getElementById("submit-changes");
        buttonTarget.append(submitChanges);


        var target1 = document.getElementById("article-content");
        target1.append(contentBox);

        var target2 = document.getElementById("article-title");
        target2.append(titleBox);
    }
</script>


</body>
</html>
