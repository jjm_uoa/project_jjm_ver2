<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@include file="style/style_head.jsp" %>
    <link rel="stylesheet" href="style/css/main-style.css" type="text/css">
    <title>Sign Up</title>
</head>
<body>

<%@include file="navbar.jsp" %>
<div class="container-fluid" id="signup-page">
    <div class="container-fluid" id="signup-float">
        <div>
            <form class="form-horizontal" action="/SignUp" method="post" enctype="multipart/form-data">
                <fieldset>

                    <legend class="legend"><strong>Sign Up.</strong></legend>
                    <hr>
                    <div class="row">

                        <div class="form-group col-md-6">
                            <label class="control-label" for="username">Username</label>
                            <div class="input col-md-12">
                                <input type="text" id="username" name="username" placeholder="Enter your username"
                                       class="form-control input-md" required>

                            </div>
                        </div>

                        <div class="form-group col-md-6">
                            <label class="control-label" id="fname" for="firstname">First Name</label>
                            <div class="input col-md-12">
                                <input id="firstname" name="firstname" type="text" placeholder="Enter your first name"
                                       class="form-control input-md" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="control-label" for="lastname">Last Name</label>
                            <div class="input col-md-12">
                                <input id="lastname" name="lastname" type="text" placeholder="Enter your last name"
                                       class="form-control input-md" required>
                            </div>
                        </div>


                        <div class="form-group col-md-6">
                            <label class="control-label" for="middlename">Middle Name(s)</label>
                            <div class="input col-md-12">
                                <input id="middlename" name="middlename" type="text"
                                       placeholder="Enter your middle name(s)"
                                       class="form-control input-md">
                            </div>
                        </div>
                    </div>
                    <div class="row">

                        <div class="form-group col-md-6">
                            <label class="control-label" for="email">Email</label>
                            <div class="input col-md-12">
                                <input id="email" name="email" type="text" placeholder="Enter your email address"
                                       class="form-control input-md" required>
                            </div>
                        </div>

                        <div class="form-group col-md-6">
                            <label class="control-label" for="birthday">Date of Birth</label>
                            <div class="input col-md-12">
                                <input type="date" id="birthday" placeholder="Your birth date"
                                       class="form-control input-md"
                                       name="birthday" required>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="country">Select Your Country: </label>
                            <br/>
                            <div class="input col-md-12">
                                <select id="country" name="country" class="form-control" required>


                                    <% String[] countriesArray = countriesList.split("_");
                                        for (int i = 0; i < countriesArray.length; i += 2) {
                                    %>
                                    <option value="<%=countriesArray[i]%>">
                                        <%=countriesArray[i + 1]%>
                                    </option>
                                    <% } %>

                                </select>
                            </div>
                        </div>
                    </div>
                    <br>


                </fieldset>

                <div class="form-group">
                    <fieldset>
                        <legend class="legend"><strong>Select avatar.</strong></legend>
                        <hr>

                        <div class="form-group">
                            <% for (int i = 1; i < 6; i++) { %>
                            <input type="radio" id="avatar<%=i%>" value="avatar<%=i%>" name="system_avatars">
                            <label for="avatar<%=i%>"> <img src="/avatars/avatar<%=i%>.png" width="50px"></label>
                            <% } %>
                        </div>

                        <div class="input col-md-8">
                            <input type="file" class="btn btn-warning" name="file">
                        </div>

                    </fieldset>
                </div>
                <div class="input form-group col-md-12">
                    <label class="control-label" for="description">Description</label>
                    <div class="input col-md-12">
                        <textarea id="description" name="description" placeholder="Enter a description"
                                  class="form-control input-md" required></textarea>

                    </div>
                </div>
                <br>

                <div class="form-group">
                    <fieldset>
                        <legend class="legend"><strong>Password.</strong></legend>
                        <hr>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="pass1">Password</label>
                                <div class="input col-md-12">
                                    <input type="password" class="form-control" id="pass1"
                                           placeholder="Enter your new Password"
                                           name="password" required>
                                </div>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="pass2">Re-enter Password</label>
                                <div class="input col-md-12">
                                    <input type="password" class="form-control" id="pass2"
                                           placeholder="Re-enter your password"
                                           name="password_confirm" required>
                                </div>
                            </div>
                        </div>
                    </fieldset>

                    <div>
                        <p id="password_reminder"></p>
                    </div>
                </div>
                <div class="input col-md-8">
                    <button type="submit" class="btn btn-primary">Register</button>
                </div>


            </form>
        </div>
    </div>
</div>
<%!
    String countriesList = "AF_Afghanistan_AL_Albania_DZ_Algeria_AS_American Samoa_AD_Andorra_AO_Angola_AI_Anguilla_AQ_Antarctica_AG_Antigua and Barbuda_AR_Argentina_AM_Armenia_AW_Aruba_AU_Australia_AT_Austria_AZ_Azerbaijan_BS_Bahamas_BH_Bahrain_BD_Bangladesh_BB_Barbados_BY_Belarus_BE_Belgium_BZ_Belize_BJ_Benin_BM_Bermuda_BT_Bhutan_BO_Bolivia_BQ_Bonaire_BA_Bosnia and Herzegovina_BW_Botswana_BV_Bouvet Island_BR_Brazil_IO_British Indian Ocean Territory_BN_Brunei Darussalam_BG_Bulgaria_BF_Burkina Faso_BI_Burundi_KH_Cambodia_CM_Cameroon_CA_Canada_CV_Cape Verde_KY_Cayman Islands_CF_Central African Republic_TD_Chad_CL_Chile_CN_China_CX_Christmas Island_CC_Cocos (Keeling) Islands_CO_Colombia_KM_Comoros_CG_Congo_CD_Democratic Republic of the Congo_CK_Cook Islands_CR_Costa Rica_HR_Croatia_CU_Cuba_CW_Curacao_CY_Cyprus_CZ_Czech Republic_CI_\"Cote d'Ivoire\"_DK_Denmark_DJ_Djibouti_DM_Dominica_DO_Dominican Republic_EC_Ecuador_EG_Egypt_SV_El Salvador_GQ_Equatorial Guinea_ER_Eritrea_EE_Estonia_ET_Ethiopia_FK_Falkland Islands (Malvinas)_FO_Faroe Islands_FJ_Fiji_FI_Finland_FR_France_GF_French Guiana_PF_French Polynesia_TF_French Southern Territories_GA_Gabon_GM_Gambia_GE_Georgia_DE_Germany_GH_Ghana_GI_Gibraltar_GR_Greece_GL_Greenland_GD_Grenada_GP_Guadeloupe_GU_Guam_GT_Guatemala_GG_Guernsey_GN_Guinea_GW_Guinea-Bissau_GY_Guyana_HT_Haiti_HM_Heard Island and McDonald Mcdonald Islands_VA_Holy See (Vatican City State)_HN_Honduras_HK_Hong Kong_HU_Hungary_IS_Iceland_IN_India_ID_Indonesia_IR_Iran, Islamic Republic of_IQ_Iraq_IE_Ireland_IM_Isle of Man_IL_Israel_IT_Italy_JM_Jamaica_JP_Japan_JE_Jersey_JO_Jordan_KZ_Kazakhstan_KE_Kenya_KI_Kiribati_KP_ \"Korea, Democratic People's Republic of_KR_Korea, Republic of_KW_Kuwait_KG_Kyrgyzstan_LA_\"Lao People's Democratic Republic\"_LV_Latvia_LB_Lebanon_LS_Lesotho_LR_Liberia_LY_Libya_LI_Liechtenstein_LT_Lithuania_LU_Luxembourg_MO_Macao_MK_Macedonia, the Former Yugoslav Republic of_MG_Madagascar_MW_Malawi_MY_Malaysia_MV_Maldives_ML_Mali_MT_Malta_MH_Marshall Islands_MQ_Martinique_MR_Mauritania_MU_Mauritius_YT_Mayotte_MX_Mexico_FM_Micronesia, Federated States of_MD_Moldova, Republic of_MC_Monaco_MN_Mongolia_ME_Montenegro_MS_Montserrat_MA_Morocco_MZ_Mozambique_MM_Myanmar_NA_Namibia_NR_Nauru_NP_Nepal_NL_Netherlands_NC_New Caledonia_NZ_New Zealand_NI_Nicaragua_NE_Niger_NG_Nigeria_NU_Niue_NF_Norfolk Island_MP_Northern Mariana Islands_NO_Norway_OM_Oman_PK_Pakistan_PW_Palau_PS_Palestine, State of_PA_Panama_PG_Papua New Guinea_PY_Paraguay_PE_Peru_PH_Philippines_PN_Pitcairn_PL_Poland_PT_Portugal_PR_Puerto Rico_QA_Qatar_RO_Romania_RU_Russian Federation_RW_Rwanda_RE_Reunion_BL_Saint Barthelemy_SH_Saint Helena_KN_Saint Kitts and Nevis_LC_Saint Lucia_MF_Saint Martin (French part)_PM_Saint Pierre and Miquelon_VC_Saint Vincent and the Grenadines_WS_Samoa_SM_San Marino_ST_Sao Tome and Principe_SA_Saudi Arabia_SN_Senegal_RS_Serbia_SC_Seychelles_SL_Sierra Leone_SG_Singapore_SX_Sint Maarten (Dutch part)_SK_Slovakia_SI_Slovenia_SB_Solomon Islands_SO_Somalia_ZA_South Africa_GS_South Georgia and the South Sandwich Islands_SS_South Sudan_ES_Spain_LK_Sri Lanka_SD_Sudan_SR_Suriname_SJ_Svalbard and Jan Mayen_SZ_Swaziland_SE_Sweden_CH_Switzerland_SY_Syrian Arab Republic_TW_Taiwan, Province of China_TJ_Tajikistan_TZ_United Republic of Tanzania_TH_Thailand_TL_Timor-Leste_TG_Togo_TK_Tokelau_TO_Tonga_TT_Trinidad and Tobago_TN_Tunisia_TR_Turkey_TM_Turkmenistan_TC_Turks and Caicos Islands_TV_Tuvalu_UG_Uganda_UA_Ukraine_AE_United Arab Emirates_GB_United Kingdom_US_United States_UM_United States Minor Outlying Islands_UY_Uruguay_UZ_Uzbekistan_VU_Vanuatu_VE_Venezuela_VN_Viet Nam_VG_British Virgin Islands_VI_US Virgin Islands_WF_Wallis and Futuna_EH_Western Sahara_YE_Yemen_ZM_Zambia_ZW_Zimbabwe";
    ;%>

</body>
</html>
