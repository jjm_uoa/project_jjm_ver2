
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Log Out Util</title>
</head>
<body>

<%
    System.err.println("   ***   Log out util   ***");
//    session.invalidate();
    session.removeAttribute("accountRole");
    session.removeAttribute("id");
    session.removeAttribute("login");
    session.removeAttribute("login_status");
    session.setAttribute("confirm_to_erase", "false");

    response.sendRedirect("/index.jsp");
%>

</body>
</html>
