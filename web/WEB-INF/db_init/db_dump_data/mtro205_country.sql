CREATE TABLE mtro205.country
(
    code varchar(2) PRIMARY KEY NOT NULL,
    name varchar(60) NOT NULL
);
CREATE UNIQUE INDEX country_code ON mtro205.country (code);
CREATE UNIQUE INDEX name ON mtro205.country (name);
INSERT INTO mtro205.country (code, name) VALUES ('AF', 'Afghanistan');
INSERT INTO mtro205.country (code, name) VALUES ('AL', 'Albania');
INSERT INTO mtro205.country (code, name) VALUES ('DZ', 'Algeria');
INSERT INTO mtro205.country (code, name) VALUES ('AS', 'American Samoa');
INSERT INTO mtro205.country (code, name) VALUES ('AD', 'Andorra');
INSERT INTO mtro205.country (code, name) VALUES ('AO', 'Angola');
INSERT INTO mtro205.country (code, name) VALUES ('AI', 'Anguilla');
INSERT INTO mtro205.country (code, name) VALUES ('AQ', 'Antarctica');
INSERT INTO mtro205.country (code, name) VALUES ('AG', 'Antigua and Barbuda');
INSERT INTO mtro205.country (code, name) VALUES ('AR', 'Argentina');
INSERT INTO mtro205.country (code, name) VALUES ('AM', 'Armenia');
INSERT INTO mtro205.country (code, name) VALUES ('AW', 'Aruba');
INSERT INTO mtro205.country (code, name) VALUES ('AU', 'Australia');
INSERT INTO mtro205.country (code, name) VALUES ('AT', 'Austria');
INSERT INTO mtro205.country (code, name) VALUES ('AZ', 'Azerbaijan');
INSERT INTO mtro205.country (code, name) VALUES ('BS', 'Bahamas');
INSERT INTO mtro205.country (code, name) VALUES ('BH', 'Bahrain');
INSERT INTO mtro205.country (code, name) VALUES ('BD', 'Bangladesh');
INSERT INTO mtro205.country (code, name) VALUES ('BB', 'Barbados');
INSERT INTO mtro205.country (code, name) VALUES ('BY', 'Belarus');
INSERT INTO mtro205.country (code, name) VALUES ('BE', 'Belgium');
INSERT INTO mtro205.country (code, name) VALUES ('BZ', 'Belize');
INSERT INTO mtro205.country (code, name) VALUES ('BJ', 'Benin');
INSERT INTO mtro205.country (code, name) VALUES ('BM', 'Bermuda');
INSERT INTO mtro205.country (code, name) VALUES ('BT', 'Bhutan');
INSERT INTO mtro205.country (code, name) VALUES ('BO', 'Bolivia');
INSERT INTO mtro205.country (code, name) VALUES ('BQ', 'Bonaire');
INSERT INTO mtro205.country (code, name) VALUES ('BA', 'Bosnia and Herzegovina');
INSERT INTO mtro205.country (code, name) VALUES ('BW', 'Botswana');
INSERT INTO mtro205.country (code, name) VALUES ('BV', 'Bouvet Island');
INSERT INTO mtro205.country (code, name) VALUES ('BR', 'Brazil');
INSERT INTO mtro205.country (code, name) VALUES ('IO', 'British Indian Ocean Territory');
INSERT INTO mtro205.country (code, name) VALUES ('VG', 'British Virgin Islands');
INSERT INTO mtro205.country (code, name) VALUES ('BN', 'Brunei Darussalam');
INSERT INTO mtro205.country (code, name) VALUES ('BG', 'Bulgaria');
INSERT INTO mtro205.country (code, name) VALUES ('BF', 'Burkina Faso');
INSERT INTO mtro205.country (code, name) VALUES ('BI', 'Burundi');
INSERT INTO mtro205.country (code, name) VALUES ('KH', 'Cambodia');
INSERT INTO mtro205.country (code, name) VALUES ('CM', 'Cameroon');
INSERT INTO mtro205.country (code, name) VALUES ('CA', 'Canada');
INSERT INTO mtro205.country (code, name) VALUES ('CV', 'Cape Verde');
INSERT INTO mtro205.country (code, name) VALUES ('KY', 'Cayman Islands');
INSERT INTO mtro205.country (code, name) VALUES ('CF', 'Central African Republic');
INSERT INTO mtro205.country (code, name) VALUES ('TD', 'Chad');
INSERT INTO mtro205.country (code, name) VALUES ('CL', 'Chile');
INSERT INTO mtro205.country (code, name) VALUES ('CN', 'China');
INSERT INTO mtro205.country (code, name) VALUES ('CX', 'Christmas Island');
INSERT INTO mtro205.country (code, name) VALUES ('CC', 'Cocos (Keeling) Islands');
INSERT INTO mtro205.country (code, name) VALUES ('CO', 'Colombia');
INSERT INTO mtro205.country (code, name) VALUES ('KM', 'Comoros');
INSERT INTO mtro205.country (code, name) VALUES ('CG', 'Congo');
INSERT INTO mtro205.country (code, name) VALUES ('CK', 'Cook Islands');
INSERT INTO mtro205.country (code, name) VALUES ('CR', 'Costa Rica');
INSERT INTO mtro205.country (code, name) VALUES ('CI', 'Cote d''Ivoire');
INSERT INTO mtro205.country (code, name) VALUES ('HR', 'Croatia');
INSERT INTO mtro205.country (code, name) VALUES ('CU', 'Cuba');
INSERT INTO mtro205.country (code, name) VALUES ('CW', 'Curacao');
INSERT INTO mtro205.country (code, name) VALUES ('CY', 'Cyprus');
INSERT INTO mtro205.country (code, name) VALUES ('CZ', 'Czech Republic');
INSERT INTO mtro205.country (code, name) VALUES ('CD', 'Democratic Republic of the Congo');
INSERT INTO mtro205.country (code, name) VALUES ('DK', 'Denmark');
INSERT INTO mtro205.country (code, name) VALUES ('DJ', 'Djibouti');
INSERT INTO mtro205.country (code, name) VALUES ('DM', 'Dominica');
INSERT INTO mtro205.country (code, name) VALUES ('DO', 'Dominican Republic');
INSERT INTO mtro205.country (code, name) VALUES ('EC', 'Ecuador');
INSERT INTO mtro205.country (code, name) VALUES ('EG', 'Egypt');
INSERT INTO mtro205.country (code, name) VALUES ('SV', 'El Salvador');
INSERT INTO mtro205.country (code, name) VALUES ('GQ', 'Equatorial Guinea');
INSERT INTO mtro205.country (code, name) VALUES ('ER', 'Eritrea');
INSERT INTO mtro205.country (code, name) VALUES ('EE', 'Estonia');
INSERT INTO mtro205.country (code, name) VALUES ('ET', 'Ethiopia');
INSERT INTO mtro205.country (code, name) VALUES ('FK', 'Falkland Islands (Malvinas)');
INSERT INTO mtro205.country (code, name) VALUES ('FO', 'Faroe Islands');
INSERT INTO mtro205.country (code, name) VALUES ('FJ', 'Fiji');
INSERT INTO mtro205.country (code, name) VALUES ('FI', 'Finland');
INSERT INTO mtro205.country (code, name) VALUES ('FR', 'France');
INSERT INTO mtro205.country (code, name) VALUES ('GF', 'French Guiana');
INSERT INTO mtro205.country (code, name) VALUES ('PF', 'French Polynesia');
INSERT INTO mtro205.country (code, name) VALUES ('TF', 'French Southern Territories');
INSERT INTO mtro205.country (code, name) VALUES ('GA', 'Gabon');
INSERT INTO mtro205.country (code, name) VALUES ('GM', 'Gambia');
INSERT INTO mtro205.country (code, name) VALUES ('GE', 'Georgia');
INSERT INTO mtro205.country (code, name) VALUES ('DE', 'Germany');
INSERT INTO mtro205.country (code, name) VALUES ('GH', 'Ghana');
INSERT INTO mtro205.country (code, name) VALUES ('GI', 'Gibraltar');
INSERT INTO mtro205.country (code, name) VALUES ('GR', 'Greece');
INSERT INTO mtro205.country (code, name) VALUES ('GL', 'Greenland');
INSERT INTO mtro205.country (code, name) VALUES ('GD', 'Grenada');
INSERT INTO mtro205.country (code, name) VALUES ('GP', 'Guadeloupe');
INSERT INTO mtro205.country (code, name) VALUES ('GU', 'Guam');
INSERT INTO mtro205.country (code, name) VALUES ('GT', 'Guatemala');
INSERT INTO mtro205.country (code, name) VALUES ('GG', 'Guernsey');
INSERT INTO mtro205.country (code, name) VALUES ('GN', 'Guinea');
INSERT INTO mtro205.country (code, name) VALUES ('GW', 'Guinea-Bissau');
INSERT INTO mtro205.country (code, name) VALUES ('GY', 'Guyana');
INSERT INTO mtro205.country (code, name) VALUES ('HT', 'Haiti');
INSERT INTO mtro205.country (code, name) VALUES ('HM', 'Heard Island and McDonald Mcdonald Islands');
INSERT INTO mtro205.country (code, name) VALUES ('VA', 'Holy See (Vatican City State)');
INSERT INTO mtro205.country (code, name) VALUES ('HN', 'Honduras');
INSERT INTO mtro205.country (code, name) VALUES ('HK', 'Hong Kong');
INSERT INTO mtro205.country (code, name) VALUES ('HU', 'Hungary');
INSERT INTO mtro205.country (code, name) VALUES ('IS', 'Iceland');
INSERT INTO mtro205.country (code, name) VALUES ('IN', 'India');
INSERT INTO mtro205.country (code, name) VALUES ('ID', 'Indonesia');
INSERT INTO mtro205.country (code, name) VALUES ('IR', 'Iran, Islamic Republic of');
INSERT INTO mtro205.country (code, name) VALUES ('IQ', 'Iraq');
INSERT INTO mtro205.country (code, name) VALUES ('IE', 'Ireland');
INSERT INTO mtro205.country (code, name) VALUES ('IM', 'Isle of Man');
INSERT INTO mtro205.country (code, name) VALUES ('IL', 'Israel');
INSERT INTO mtro205.country (code, name) VALUES ('IT', 'Italy');
INSERT INTO mtro205.country (code, name) VALUES ('JM', 'Jamaica');
INSERT INTO mtro205.country (code, name) VALUES ('JP', 'Japan');
INSERT INTO mtro205.country (code, name) VALUES ('JE', 'Jersey');
INSERT INTO mtro205.country (code, name) VALUES ('JO', 'Jordan');
INSERT INTO mtro205.country (code, name) VALUES ('KZ', 'Kazakhstan');
INSERT INTO mtro205.country (code, name) VALUES ('KE', 'Kenya');
INSERT INTO mtro205.country (code, name) VALUES ('KI', 'Kiribati');
INSERT INTO mtro205.country (code, name) VALUES ('KP', 'Korea, Democratic People''s Republic of');
INSERT INTO mtro205.country (code, name) VALUES ('KR', 'Korea, Republic of');
INSERT INTO mtro205.country (code, name) VALUES ('KW', 'Kuwait');
INSERT INTO mtro205.country (code, name) VALUES ('KG', 'Kyrgyzstan');
INSERT INTO mtro205.country (code, name) VALUES ('LA', 'Lao People''s Democratic Republic');
INSERT INTO mtro205.country (code, name) VALUES ('LV', 'Latvia');
INSERT INTO mtro205.country (code, name) VALUES ('LB', 'Lebanon');
INSERT INTO mtro205.country (code, name) VALUES ('LS', 'Lesotho');
INSERT INTO mtro205.country (code, name) VALUES ('LR', 'Liberia');
INSERT INTO mtro205.country (code, name) VALUES ('LY', 'Libya');
INSERT INTO mtro205.country (code, name) VALUES ('LI', 'Liechtenstein');
INSERT INTO mtro205.country (code, name) VALUES ('LT', 'Lithuania');
INSERT INTO mtro205.country (code, name) VALUES ('LU', 'Luxembourg');
INSERT INTO mtro205.country (code, name) VALUES ('MO', 'Macao');
INSERT INTO mtro205.country (code, name) VALUES ('MK', 'Macedonia, the Former Yugoslav Republic of');
INSERT INTO mtro205.country (code, name) VALUES ('MG', 'Madagascar');
INSERT INTO mtro205.country (code, name) VALUES ('MW', 'Malawi');
INSERT INTO mtro205.country (code, name) VALUES ('MY', 'Malaysia');
INSERT INTO mtro205.country (code, name) VALUES ('MV', 'Maldives');
INSERT INTO mtro205.country (code, name) VALUES ('ML', 'Mali');
INSERT INTO mtro205.country (code, name) VALUES ('MT', 'Malta');
INSERT INTO mtro205.country (code, name) VALUES ('MH', 'Marshall Islands');
INSERT INTO mtro205.country (code, name) VALUES ('MQ', 'Martinique');
INSERT INTO mtro205.country (code, name) VALUES ('MR', 'Mauritania');
INSERT INTO mtro205.country (code, name) VALUES ('MU', 'Mauritius');
INSERT INTO mtro205.country (code, name) VALUES ('YT', 'Mayotte');
INSERT INTO mtro205.country (code, name) VALUES ('MX', 'Mexico');
INSERT INTO mtro205.country (code, name) VALUES ('FM', 'Micronesia, Federated States of');
INSERT INTO mtro205.country (code, name) VALUES ('MD', 'Moldova, Republic of');
INSERT INTO mtro205.country (code, name) VALUES ('MC', 'Monaco');
INSERT INTO mtro205.country (code, name) VALUES ('MN', 'Mongolia');
INSERT INTO mtro205.country (code, name) VALUES ('ME', 'Montenegro');
INSERT INTO mtro205.country (code, name) VALUES ('MS', 'Montserrat');
INSERT INTO mtro205.country (code, name) VALUES ('MA', 'Morocco');
INSERT INTO mtro205.country (code, name) VALUES ('MZ', 'Mozambique');
INSERT INTO mtro205.country (code, name) VALUES ('MM', 'Myanmar');
INSERT INTO mtro205.country (code, name) VALUES ('NA', 'Namibia');
INSERT INTO mtro205.country (code, name) VALUES ('NR', 'Nauru');
INSERT INTO mtro205.country (code, name) VALUES ('NP', 'Nepal');
INSERT INTO mtro205.country (code, name) VALUES ('NL', 'Netherlands');
INSERT INTO mtro205.country (code, name) VALUES ('NC', 'New Caledonia');
INSERT INTO mtro205.country (code, name) VALUES ('NZ', 'New Zealand');
INSERT INTO mtro205.country (code, name) VALUES ('NI', 'Nicaragua');
INSERT INTO mtro205.country (code, name) VALUES ('NE', 'Niger');
INSERT INTO mtro205.country (code, name) VALUES ('NG', 'Nigeria');
INSERT INTO mtro205.country (code, name) VALUES ('NU', 'Niue');
INSERT INTO mtro205.country (code, name) VALUES ('NF', 'Norfolk Island');
INSERT INTO mtro205.country (code, name) VALUES ('MP', 'Northern Mariana Islands');
INSERT INTO mtro205.country (code, name) VALUES ('NO', 'Norway');
INSERT INTO mtro205.country (code, name) VALUES ('OM', 'Oman');
INSERT INTO mtro205.country (code, name) VALUES ('PK', 'Pakistan');
INSERT INTO mtro205.country (code, name) VALUES ('PW', 'Palau');
INSERT INTO mtro205.country (code, name) VALUES ('PS', 'Palestine, State of');
INSERT INTO mtro205.country (code, name) VALUES ('PA', 'Panama');
INSERT INTO mtro205.country (code, name) VALUES ('PG', 'Papua New Guinea');
INSERT INTO mtro205.country (code, name) VALUES ('PY', 'Paraguay');
INSERT INTO mtro205.country (code, name) VALUES ('PE', 'Peru');
INSERT INTO mtro205.country (code, name) VALUES ('PH', 'Philippines');
INSERT INTO mtro205.country (code, name) VALUES ('PN', 'Pitcairn');
INSERT INTO mtro205.country (code, name) VALUES ('PL', 'Poland');
INSERT INTO mtro205.country (code, name) VALUES ('PT', 'Portugal');
INSERT INTO mtro205.country (code, name) VALUES ('PR', 'Puerto Rico');
INSERT INTO mtro205.country (code, name) VALUES ('QA', 'Qatar');
INSERT INTO mtro205.country (code, name) VALUES ('RE', 'Reunion');
INSERT INTO mtro205.country (code, name) VALUES ('RO', 'Romania');
INSERT INTO mtro205.country (code, name) VALUES ('RU', 'Russian Federation');
INSERT INTO mtro205.country (code, name) VALUES ('RW', 'Rwanda');
INSERT INTO mtro205.country (code, name) VALUES ('BL', 'Saint Barthelemy');
INSERT INTO mtro205.country (code, name) VALUES ('SH', 'Saint Helena');
INSERT INTO mtro205.country (code, name) VALUES ('KN', 'Saint Kitts and Nevis');
INSERT INTO mtro205.country (code, name) VALUES ('LC', 'Saint Lucia');
INSERT INTO mtro205.country (code, name) VALUES ('MF', 'Saint Martin (French part)');
INSERT INTO mtro205.country (code, name) VALUES ('PM', 'Saint Pierre and Miquelon');
INSERT INTO mtro205.country (code, name) VALUES ('VC', 'Saint Vincent and the Grenadines');
INSERT INTO mtro205.country (code, name) VALUES ('WS', 'Samoa');
INSERT INTO mtro205.country (code, name) VALUES ('SM', 'San Marino');
INSERT INTO mtro205.country (code, name) VALUES ('ST', 'Sao Tome and Principe');
INSERT INTO mtro205.country (code, name) VALUES ('SA', 'Saudi Arabia');
INSERT INTO mtro205.country (code, name) VALUES ('SN', 'Senegal');
INSERT INTO mtro205.country (code, name) VALUES ('RS', 'Serbia');
INSERT INTO mtro205.country (code, name) VALUES ('SC', 'Seychelles');
INSERT INTO mtro205.country (code, name) VALUES ('SL', 'Sierra Leone');
INSERT INTO mtro205.country (code, name) VALUES ('SG', 'Singapore');
INSERT INTO mtro205.country (code, name) VALUES ('SX', 'Sint Maarten (Dutch part)');
INSERT INTO mtro205.country (code, name) VALUES ('SK', 'Slovakia');
INSERT INTO mtro205.country (code, name) VALUES ('SI', 'Slovenia');
INSERT INTO mtro205.country (code, name) VALUES ('SB', 'Solomon Islands');
INSERT INTO mtro205.country (code, name) VALUES ('SO', 'Somalia');
INSERT INTO mtro205.country (code, name) VALUES ('ZA', 'South Africa');
INSERT INTO mtro205.country (code, name) VALUES ('GS', 'South Georgia and the South Sandwich Islands');
INSERT INTO mtro205.country (code, name) VALUES ('SS', 'South Sudan');
INSERT INTO mtro205.country (code, name) VALUES ('ES', 'Spain');
INSERT INTO mtro205.country (code, name) VALUES ('LK', 'Sri Lanka');
INSERT INTO mtro205.country (code, name) VALUES ('SD', 'Sudan');
INSERT INTO mtro205.country (code, name) VALUES ('SR', 'Suriname');
INSERT INTO mtro205.country (code, name) VALUES ('SJ', 'Svalbard and Jan Mayen');
INSERT INTO mtro205.country (code, name) VALUES ('SZ', 'Swaziland');
INSERT INTO mtro205.country (code, name) VALUES ('SE', 'Sweden');
INSERT INTO mtro205.country (code, name) VALUES ('CH', 'Switzerland');
INSERT INTO mtro205.country (code, name) VALUES ('SY', 'Syrian Arab Republic');
INSERT INTO mtro205.country (code, name) VALUES ('TW', 'Taiwan, Province of China');
INSERT INTO mtro205.country (code, name) VALUES ('TJ', 'Tajikistan');
INSERT INTO mtro205.country (code, name) VALUES ('TH', 'Thailand');
INSERT INTO mtro205.country (code, name) VALUES ('TL', 'Timor-Leste');
INSERT INTO mtro205.country (code, name) VALUES ('TG', 'Togo');
INSERT INTO mtro205.country (code, name) VALUES ('TK', 'Tokelau');
INSERT INTO mtro205.country (code, name) VALUES ('TO', 'Tonga');
INSERT INTO mtro205.country (code, name) VALUES ('TT', 'Trinidad and Tobago');
INSERT INTO mtro205.country (code, name) VALUES ('TN', 'Tunisia');
INSERT INTO mtro205.country (code, name) VALUES ('TR', 'Turkey');
INSERT INTO mtro205.country (code, name) VALUES ('TM', 'Turkmenistan');
INSERT INTO mtro205.country (code, name) VALUES ('TC', 'Turks and Caicos Islands');
INSERT INTO mtro205.country (code, name) VALUES ('TV', 'Tuvalu');
INSERT INTO mtro205.country (code, name) VALUES ('UG', 'Uganda');
INSERT INTO mtro205.country (code, name) VALUES ('UA', 'Ukraine');
INSERT INTO mtro205.country (code, name) VALUES ('AE', 'United Arab Emirates');
INSERT INTO mtro205.country (code, name) VALUES ('GB', 'United Kingdom');
INSERT INTO mtro205.country (code, name) VALUES ('TZ', 'United Republic of Tanzania');
INSERT INTO mtro205.country (code, name) VALUES ('US', 'United States');
INSERT INTO mtro205.country (code, name) VALUES ('UM', 'United States Minor Outlying Islands');
INSERT INTO mtro205.country (code, name) VALUES ('UY', 'Uruguay');
INSERT INTO mtro205.country (code, name) VALUES ('VI', 'US Virgin Islands');
INSERT INTO mtro205.country (code, name) VALUES ('UZ', 'Uzbekistan');
INSERT INTO mtro205.country (code, name) VALUES ('VU', 'Vanuatu');
INSERT INTO mtro205.country (code, name) VALUES ('VE', 'Venezuela');
INSERT INTO mtro205.country (code, name) VALUES ('VN', 'Viet Nam');
INSERT INTO mtro205.country (code, name) VALUES ('WF', 'Wallis and Futuna');
INSERT INTO mtro205.country (code, name) VALUES ('EH', 'Western Sahara');
INSERT INTO mtro205.country (code, name) VALUES ('YE', 'Yemen');
INSERT INTO mtro205.country (code, name) VALUES ('ZM', 'Zambia');
INSERT INTO mtro205.country (code, name) VALUES ('ZW', 'Zimbabwe');