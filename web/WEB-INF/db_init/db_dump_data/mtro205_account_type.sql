CREATE TABLE mtro205.account_type
(
    id int(11) PRIMARY KEY NOT NULL,
    type_id int(11) NOT NULL,
    description varchar(100),
    CONSTRAINT account FOREIGN KEY (id) REFERENCES mtro205.account (id)
);
CREATE INDEX id ON mtro205.account_type (id);
INSERT INTO mtro205.account_type (id, type_id, description) VALUES (72, 2, 'user');
INSERT INTO mtro205.account_type (id, type_id, description) VALUES (73, 2, 'user');
INSERT INTO mtro205.account_type (id, type_id, description) VALUES (74, 2, 'user');
INSERT INTO mtro205.account_type (id, type_id, description) VALUES (77, 2, 'user');
INSERT INTO mtro205.account_type (id, type_id, description) VALUES (80, 2, 'user');
INSERT INTO mtro205.account_type (id, type_id, description) VALUES (81, 2, 'user');
INSERT INTO mtro205.account_type (id, type_id, description) VALUES (82, 2, 'user');
INSERT INTO mtro205.account_type (id, type_id, description) VALUES (83, 1, 'user');
INSERT INTO mtro205.account_type (id, type_id, description) VALUES (85, 2, 'user');
INSERT INTO mtro205.account_type (id, type_id, description) VALUES (86, 2, 'user');
INSERT INTO mtro205.account_type (id, type_id, description) VALUES (91, 2, 'user');
INSERT INTO mtro205.account_type (id, type_id, description) VALUES (92, 2, 'user');
INSERT INTO mtro205.account_type (id, type_id, description) VALUES (102, 2, 'user');
INSERT INTO mtro205.account_type (id, type_id, description) VALUES (103, 2, 'user');
INSERT INTO mtro205.account_type (id, type_id, description) VALUES (104, 2, 'user');
INSERT INTO mtro205.account_type (id, type_id, description) VALUES (108, 2, 'user');
INSERT INTO mtro205.account_type (id, type_id, description) VALUES (109, 2, 'user');
INSERT INTO mtro205.account_type (id, type_id, description) VALUES (114, 2, 'user');
INSERT INTO mtro205.account_type (id, type_id, description) VALUES (115, 2, 'user');
INSERT INTO mtro205.account_type (id, type_id, description) VALUES (116, 2, 'user');
INSERT INTO mtro205.account_type (id, type_id, description) VALUES (117, 2, 'user');
INSERT INTO mtro205.account_type (id, type_id, description) VALUES (118, 2, 'user');
INSERT INTO mtro205.account_type (id, type_id, description) VALUES (119, 2, 'user');
INSERT INTO mtro205.account_type (id, type_id, description) VALUES (127, 2, 'user');
INSERT INTO mtro205.account_type (id, type_id, description) VALUES (128, 2, 'user');
INSERT INTO mtro205.account_type (id, type_id, description) VALUES (131, 2, 'user');
INSERT INTO mtro205.account_type (id, type_id, description) VALUES (132, 2, 'user');
INSERT INTO mtro205.account_type (id, type_id, description) VALUES (146, 2, 'user');
INSERT INTO mtro205.account_type (id, type_id, description) VALUES (147, 2, 'user');
INSERT INTO mtro205.account_type (id, type_id, description) VALUES (158, 2, 'user');
INSERT INTO mtro205.account_type (id, type_id, description) VALUES (159, 2, 'user');
INSERT INTO mtro205.account_type (id, type_id, description) VALUES (164, 2, 'user');
INSERT INTO mtro205.account_type (id, type_id, description) VALUES (165, 2, 'user');
INSERT INTO mtro205.account_type (id, type_id, description) VALUES (168, 2, 'user');
INSERT INTO mtro205.account_type (id, type_id, description) VALUES (169, 2, 'user');
INSERT INTO mtro205.account_type (id, type_id, description) VALUES (174, 2, 'user');
INSERT INTO mtro205.account_type (id, type_id, description) VALUES (175, 2, 'user');
INSERT INTO mtro205.account_type (id, type_id, description) VALUES (178, 2, 'user');
INSERT INTO mtro205.account_type (id, type_id, description) VALUES (181, 2, 'user');
INSERT INTO mtro205.account_type (id, type_id, description) VALUES (184, 2, 'user');
INSERT INTO mtro205.account_type (id, type_id, description) VALUES (233, 2, 'user');
INSERT INTO mtro205.account_type (id, type_id, description) VALUES (234, 2, 'user');
INSERT INTO mtro205.account_type (id, type_id, description) VALUES (241, 2, 'user');
INSERT INTO mtro205.account_type (id, type_id, description) VALUES (243, 2, 'user');
INSERT INTO mtro205.account_type (id, type_id, description) VALUES (305, 2, 'user');
INSERT INTO mtro205.account_type (id, type_id, description) VALUES (306, 2, 'user');
INSERT INTO mtro205.account_type (id, type_id, description) VALUES (307, 2, 'user');
INSERT INTO mtro205.account_type (id, type_id, description) VALUES (308, 2, 'user');
INSERT INTO mtro205.account_type (id, type_id, description) VALUES (309, 2, 'user');
INSERT INTO mtro205.account_type (id, type_id, description) VALUES (311, 2, 'user');
INSERT INTO mtro205.account_type (id, type_id, description) VALUES (312, 2, 'user');
INSERT INTO mtro205.account_type (id, type_id, description) VALUES (313, 2, 'user');
INSERT INTO mtro205.account_type (id, type_id, description) VALUES (314, 2, 'user');
INSERT INTO mtro205.account_type (id, type_id, description) VALUES (315, 2, 'user');
INSERT INTO mtro205.account_type (id, type_id, description) VALUES (316, 2, 'user');
INSERT INTO mtro205.account_type (id, type_id, description) VALUES (317, 2, 'user');
INSERT INTO mtro205.account_type (id, type_id, description) VALUES (318, 2, 'user');
INSERT INTO mtro205.account_type (id, type_id, description) VALUES (319, 2, 'user');
INSERT INTO mtro205.account_type (id, type_id, description) VALUES (320, 2, 'user');
INSERT INTO mtro205.account_type (id, type_id, description) VALUES (321, 2, 'user');
INSERT INTO mtro205.account_type (id, type_id, description) VALUES (322, 2, 'user');
INSERT INTO mtro205.account_type (id, type_id, description) VALUES (323, 2, 'user');
INSERT INTO mtro205.account_type (id, type_id, description) VALUES (324, 2, 'user');
INSERT INTO mtro205.account_type (id, type_id, description) VALUES (325, 2, 'user');
INSERT INTO mtro205.account_type (id, type_id, description) VALUES (326, 2, 'user');
INSERT INTO mtro205.account_type (id, type_id, description) VALUES (328, 2, 'user');
INSERT INTO mtro205.account_type (id, type_id, description) VALUES (333, 2, 'user');
INSERT INTO mtro205.account_type (id, type_id, description) VALUES (339, 2, 'user');
INSERT INTO mtro205.account_type (id, type_id, description) VALUES (340, 2, 'user');
INSERT INTO mtro205.account_type (id, type_id, description) VALUES (341, 2, 'user');
INSERT INTO mtro205.account_type (id, type_id, description) VALUES (342, 2, 'user');
INSERT INTO mtro205.account_type (id, type_id, description) VALUES (345, 1, 'admin');
INSERT INTO mtro205.account_type (id, type_id, description) VALUES (346, 2, 'user');
INSERT INTO mtro205.account_type (id, type_id, description) VALUES (347, 2, 'user');
INSERT INTO mtro205.account_type (id, type_id, description) VALUES (356, 2, 'user');
INSERT INTO mtro205.account_type (id, type_id, description) VALUES (361, 2, 'user');