CREATE TABLE mtro205.comments
(
    comment_id int(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    article_id int(11) NOT NULL,
    user_id int(11) NOT NULL,
    comment_content longtext NOT NULL,
    created_time datetime DEFAULT CURRENT_TIMESTAMP NOT NULL,
    modified_time datetime DEFAULT CURRENT_TIMESTAMP NOT NULL,
    hidden int(11) DEFAULT '0' NOT NULL,
    CONSTRAINT comments_ibfk_1 FOREIGN KEY (article_id) REFERENCES mtro205.article (article_id),
    CONSTRAINT comments_ibfk_2 FOREIGN KEY (user_id) REFERENCES mtro205.account (id)
);
CREATE INDEX article_id ON mtro205.comments (article_id);
CREATE INDEX user_id ON mtro205.comments (user_id);
INSERT INTO mtro205.comments (comment_id, article_id, user_id, comment_content, created_time, modified_time, hidden) VALUES (3, 2, 4, 'this is another comment', '2018-05-24 08:32:35', '2018-05-29 09:58:40', 0);
INSERT INTO mtro205.comments (comment_id, article_id, user_id, comment_content, created_time, modified_time, hidden) VALUES (4, 28, 52, 'larry''s comment', '2018-05-25 00:01:14', '2018-05-29 09:02:38', 0);
INSERT INTO mtro205.comments (comment_id, article_id, user_id, comment_content, created_time, modified_time, hidden) VALUES (12, 48, 74, 'this article only', '2018-05-26 23:35:47', '2018-05-26 23:35:47', 0);
INSERT INTO mtro205.comments (comment_id, article_id, user_id, comment_content, created_time, modified_time, hidden) VALUES (13, 49, 74, 'yo', '2018-05-26 23:48:14', '2018-05-26 23:48:14', 0);
INSERT INTO mtro205.comments (comment_id, article_id, user_id, comment_content, created_time, modified_time, hidden) VALUES (15, 48, 74, 'just this', '2018-05-27 00:04:17', '2018-05-29 08:16:26', 0);
INSERT INTO mtro205.comments (comment_id, article_id, user_id, comment_content, created_time, modified_time, hidden) VALUES (34, 44, 74, 'i like this song', '2018-05-27 09:45:27', '2018-05-27 09:45:27', 0);
INSERT INTO mtro205.comments (comment_id, article_id, user_id, comment_content, created_time, modified_time, hidden) VALUES (35, 41, 74, 'this article is so boring', '2018-05-27 09:48:26', '2018-05-27 09:48:26', 0);
INSERT INTO mtro205.comments (comment_id, article_id, user_id, comment_content, created_time, modified_time, hidden) VALUES (36, 41, 74, 'you are boring', '2018-05-27 09:48:46', '2018-05-27 09:48:46', 0);
INSERT INTO mtro205.comments (comment_id, article_id, user_id, comment_content, created_time, modified_time, hidden) VALUES (38, 35, 74, 'first comment', '2018-05-27 19:03:07', '2018-05-29 08:33:04', 0);
INSERT INTO mtro205.comments (comment_id, article_id, user_id, comment_content, created_time, modified_time, hidden) VALUES (65, 50, 74, 'this is working', '2018-05-29 04:15:00', '2018-05-29 04:15:00', 0);
INSERT INTO mtro205.comments (comment_id, article_id, user_id, comment_content, created_time, modified_time, hidden) VALUES (72, 2, 5, 'user 5 commented', '2018-05-29 06:02:46', '2018-05-29 09:58:40', 0);
INSERT INTO mtro205.comments (comment_id, article_id, user_id, comment_content, created_time, modified_time, hidden) VALUES (73, 2, 5, 'user 5 commented #2', '2018-05-29 06:02:46', '2018-05-29 09:58:40', 0);
INSERT INTO mtro205.comments (comment_id, article_id, user_id, comment_content, created_time, modified_time, hidden) VALUES (75, 3, 5, 'comments on fuel article', '2018-05-29 06:40:25', '2018-05-29 09:02:38', 0);
INSERT INTO mtro205.comments (comment_id, article_id, user_id, comment_content, created_time, modified_time, hidden) VALUES (76, 64, 361, 'adding some coments for styling', '2018-05-29 08:01:53', '2018-05-29 08:01:53', 0);
INSERT INTO mtro205.comments (comment_id, article_id, user_id, comment_content, created_time, modified_time, hidden) VALUES (79, 63, 74, 'can i delete?
', '2018-05-29 10:07:56', '2018-05-29 10:07:56', 0);