drop TABLE IF EXISTS password_specs;
drop table if EXISTS account;
drop table IF EXISTS country;
drop table if EXISTS article;
drop table if exists account_type;


CREATE TABLE country (
  code VARCHAR(2)  NOT NULL,
  name VARCHAR(60) NOT NULL,
  PRIMARY KEY (code),
  UNIQUE INDEX country_code (code ASC),
  UNIQUE INDEX name (name ASC)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;


CREATE TABLE account (
  id            int(11)      NOT NULL AUTO_INCREMENT,
  login         varchar(60)  NOT NULL,
  firstname     varchar(60)  NOT NULL,
  lastname      varchar(60)  NOT NULL,
  middlename    varchar(100)          DEFAULT NULL,
  password_hash varchar(100) NOT NULL,
  email         varchar(80)  NOT NULL,
  birthday      date         NOT NULL,
  country       varchar(2)   NOT NULL,
  avatar_path   varchar(255) NOT NULL,
  about         varchar(500) NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY username_UNIQUE (login),
  UNIQUE KEY email_UNIQUE (email),
  KEY code (country),
  CONSTRAINT country FOREIGN KEY (country) REFERENCES country (code)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;


CREATE TABLE password_specs (
  id         int(11) NOT NULL,
  salt       BLOB    NOT NULL,
  iterations int(11) NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT password_specs FOREIGN KEY (id) REFERENCES account (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;


CREATE TABLE article (
  article_id    int(11)                            NOT NULL AUTO_INCREMENT,
  user_id       int(11)                            NOT NULL,
  title         varchar(255)                       NOT NULL,
  content       longtext                           NOT NULL,

  created_time  datetime                           NOT NULL DEFAULT CURRENT_TIMESTAMP,
  modified_time datetime                           NOT NULL DEFAULT CURRENT_TIMESTAMP
  ON UPDATE CURRENT_TIMESTAMP,
  hidden        int default '0'                    not null,
  PRIMARY KEY (article_id),
  KEY (user_id),
  CONSTRAINT author FOREIGN KEY (user_id) REFERENCES account (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

ALTER TABLE article ADD UserDate datetime DEFAULT CURRENT_TIMESTAMP;

drop table if EXISTS comments;
CREATE TABLE comments (
  comment_id      INT AUTO_INCREMENT NOT NULL,
  article_id      INT                NOT NULL,
  user_id         INT                NOT NULL,
  comment_content LONGTEXT           NOT NULL,
  created_time    datetime           NOT NULL DEFAULT CURRENT_TIMESTAMP,
  modified_time   datetime           NOT NULL DEFAULT CURRENT_TIMESTAMP
  ON UPDATE CURRENT_TIMESTAMP,
  hidden        int default '0'                    not null,
  PRIMARY KEY (comment_id),
  FOREIGN KEY (article_id) REFERENCES article (article_id),
  FOREIGN KEY (user_id) REFERENCES account (id)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;


drop table if EXISTS finalProjectArticle;
CREATE TABLE finalProjectArticle (
  article_id   int AUTO_INCREMENT not null,
  author_fname VARCHAR(60)        NOT NULL,
  user_id      int                not null,
  author_lname VARCHAR(60),
  title        VARCHAR(60),
  content      VARCHAR(2000),
  created_time TIMESTAMP,
  PRIMARY KEY (article_id),
  FOREIGN KEY (user_id) REFERENCES account (id)
);


CREATE TABLE account_type (
  id          int(11) NOT NULL,
  type_id     int(11) NOT NULL,
  description varchar(100) DEFAULT NULL,
  PRIMARY KEY (id),
  KEY (id),
  CONSTRAINT `account` FOREIGN KEY (id) REFERENCES account (id)
    on DELETE no action
    on update no action
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;













