<%@ page import="java.util.*" %>
<%@ page import="DAO.PasswordDAO" %>
<%@ page import="DAO.AccountDAO" %>
<%@ page import="static Utils.Passwords.getNextSalt" %>
<%@ page import="static Utils.Passwords.hash" %>
<%@ page import="static Utils.Passwords.base64Encode" %>
<%--<%@ page import="DAO.AccountDAO" %>--%>
<%--<%@ page import="servlets.SignUp.*" %>--%>
<%--<%@ page import="servlets.SignUp" %>--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Admin: add users</title>
</head>
<body>


<%!
    HashMap<String, String> row;
    List<HashMap<String, String>> rows = new ArrayList<>();
    String realPath;
    int counter = 0;
    AccountDAO accountDAO;
    PasswordDAO passwordDAO;
    boolean status_completed;
    int counterC = 0;

%>

<%

    if (session.getAttribute("accountRole") != null) {


        String role = (String) session.getAttribute("accountRole");
//    String login = (String) session.getAttribute("login");

        System.err.println("role type is: " + role);

        if (!role.equals("1")) {
            System.out.println("Not admin rights for user while accessing admin page, redirection to index.jsp");
            response.sendRedirect("/index.jsp");
        } else {

%>

<span style="font-size: 30px">Admin: ADD users.</span>
<% if (counterC >= 2) {
%> <span style="color: red; font-size: 30px"> &nbsp; &nbsp; STATUS: COMPLETED!</span><%
        counterC = 0;
    }%>
<br><br>


<div>
    <br><br>
    <fieldset style="width: 25%">
        <legend>Adjust table</legend>
        <strong>
            <button id="del">-</button> &nbsp;
            <button id="add">+</button>
            &nbsp; &nbsp; &nbsp; Start index# &nbsp;

            <input id="counter_start" style="background: #AFB3B5; width: 5em" type="number" placeholder="start#"
                   value="1">
        </strong>
    </fieldset>
    <br><br>

    <form action="/admin_add_users.jsp" method="post">

        <button type="submit">ADD USERS</button> &nbsp; &nbsp;
        <button type="button" onclick="location.href='/index.jsp'">MAIN PAGE</button>
        <br><br>


        <table id="usersTable" border="1" width="100%">
            <tr style="background: #C3C7C9">
                <th>#</th>
                <th>Login</th>
                <th>Password</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Middle Name</th>
                <th>E-mail</th>
                <th>Birthday</th>
                <th>Country</th>
                <th>Avatar</th>
                <th>About</th>
            </tr>

            <tr>
                <td id="number"></td>
                <td><input type="text" name="login1" placeholder="Login" value="jjm1"></td>
                <td><input type="text" name="password1" placeholder="password" value="jjM1"></td>
                <td><input type="text" name="fname1" placeholder="First Name" value="fname1"></td>
                <td><input type="text" name="lname1" placeholder="Last Name" value="lname1"></td>
                <td><input type="text" name="mname1" placeholder="Middle Name" value="mname1"></td>
                <td><input type="text" name="mail1" placeholder="E-mail" value="mail1"></td>
                <td><input type="date" name="bday1" placeholder="Birth Day" value="2010-10-10"></td>
                <td><input type="text" name="country1" placeholder="Country" value="NZ"></td>
                <td><input type="text" name="avatar1" value="/avatars/avatar1.png" placeholder="avatar path"></td>
                <td><input type="text" name="about1" placeholder="About" value="about1"></td>
            </tr>
        </table>

    </form>

</div>

<br>


<br>


<script>

    var table = document.getElementById('usersTable');
    var counter_start = document.getElementById("counter_start");
    var counter = 2;
    document.getElementById('number').innerHTML = "<strong>&nbsp" + (counter - 1) + ".&nbsp</strong>"


    document.getElementById('add').onclick = function () {

        var shift = counter + parseInt(counter_start.value) - 1;

        console.log(counter_start.value);

        // var table = document.getElementById('usersTable');
        var row = table.insertRow(counter);
        var cell_1 = row.insertCell(0);
        var cell_2 = row.insertCell(1);
        var cell_3 = row.insertCell(2);
        var cell_4 = row.insertCell(3);
        var cell_5 = row.insertCell(4);
        var cell_6 = row.insertCell(5);
        var cell_7 = row.insertCell(6);
        var cell_8 = row.insertCell(7);
        var cell_9 = row.insertCell(8);
        var cell_10 = row.insertCell(9);
        var cell_11 = row.insertCell(10);
        cell_1.innerHTML = "<strong>&nbsp" + counter + ".&nbsp</strong>";
        cell_2.innerHTML = "<input type=\"text\" name=\"login" + counter + "\" placeholder=\"Login\" value=\"jjm" + shift + "\">";
        cell_3.innerHTML = "<input type=\"text\" name=\"password" + counter + "\" placeholder=\"password\" value=\"jjM" + shift + "\">";
        cell_4.innerHTML = "<input type=\"text\" name=\"fname" + counter + "\" placeholder=\"First Name\" value=\"fname" + shift + "\">";
        cell_5.innerHTML = "<input type=\"text\" name=\"lname" + counter + "\" placeholder=\"Last Name\" value=\"lname" + shift + "\">";
        cell_6.innerHTML = "<input type=\"text\" name=\"mname" + counter + "\" placeholder=\"Middle Name\" value=\"mname" + shift + "\">";
        cell_7.innerHTML = "<input type=\"text\" name=\"mail" + counter + "\" placeholder=\"E-mail\" value=\"mail" + shift + "\">";
        cell_8.innerHTML = "<input type=\"date\" name=\"bday" + counter + "\" placeholder=\"Birth Day\" value=\"2010-10-10\">";
        cell_9.innerHTML = "<input type=\"text\" name=\"country" + counter + "\" value=\"NZ\" placeholder=\"Country\">";
        cell_10.innerHTML = "<input type=\"text\" name=\"avatar" + counter + "\" value=\"/avatars/avatar1.png\" placeholder=\"avatar path\">";
        cell_11.innerHTML = "<input type=\"text\" name=\"about" + counter + "\" placeholder=\"About\" value=\"about" + shift + "\">";
        // counter++;
        counter++;
    }

    document.getElementById('del').onclick = function () {

        if (counter > 1) {
            table.deleteRow(counter - 1);
            counter--;
        }
    }


</script>


<%
    System.err.println("\n   ***   Admin module: add users");
    realPath = application.getRealPath(application.getInitParameter("sql_config"));
    accountDAO = new AccountDAO(realPath);
    passwordDAO = new PasswordDAO(realPath);

    if (!status_completed) {
        Map<String, String[]> map = request.getParameterMap();
        Set set = map.entrySet();
        Iterator it = set.iterator();
        int mapCounter = 1;
//    it.hasNext();
        while (it.hasNext()) {
            Map.Entry<String, String[]> entry = (Map.Entry<String, String[]>) it.next();
//        System.out.print(entry.getKey() + ", " + request.getParameter(entry.getKey()));
//        System.out.println("--------");

            if (mapCounter == 1)
                row = new HashMap<>();
            row.put(entry.getKey().replaceAll("\\d", ""), request.getParameter(entry.getKey()));


            if (mapCounter == 10) {
                rows.add(row);
                mapCounter = 0;
            }
            mapCounter++;
        }
    }

    System.err.println("read total rows from table: " + rows.size());
    for (int i = 0; i < rows.size(); i++) {
        System.err.print("  ~ row#" + i + ": ");
        HashMap<String, String> row = rows.get(i);
        for (Map.Entry<String, String> entry : row.entrySet()) {
            System.err.print(entry.getKey() + " - " + entry.getValue() + "  ;  ");
        }
        System.err.println();
    }


    //   ***   CREATE BULK OF USERS   ***

    if (!status_completed) {
        for (int i = 0; i < rows.size(); i++) {
            status_completed = true;

            String login = rows.get(i).get("login");
            System.err.println("  ~ create user#" + i + " , login: " + login);
            HashMap<String, String> row = rows.get(i);
//        for (Map.Entry<String, String> entry : row.entrySet()){
//            System.err.print(entry.getKey() + " - " + entry.getValue() + "  ;  ");

            // calculate hash
            Random random = new Random();
            int iterations = (random.nextInt(33) + 32);
            byte[] salt = getNextSalt(iterations);
            String password = rows.get(i).get("password");
            char[] charArray = password.toCharArray();
            byte[] hashedPassword = hash(charArray, salt, iterations);
            String passwordHashedString = base64Encode(hashedPassword);

            System.err.println("HASH: " + passwordHashedString);

            //   ***   rewrite HASHMAP for compatibility
            Map<String, String> attributeNames = new HashMap<>();
            attributeNames.put("username", row.get("login"));
            attributeNames.put("firstname", row.get("fname"));
            attributeNames.put("lastname", row.get("lname"));
            attributeNames.put("middlename", row.get("mname"));
            attributeNames.put("country", row.get("country"));
            attributeNames.put("password_hash", passwordHashedString);
            attributeNames.put("email", row.get("mail"));
            attributeNames.put("birthday", row.get("bday"));
            attributeNames.put("description", row.get("about"));
            attributeNames.put("file", row.get("avatar"));

            //store all fields in account database
            int id = accountDAO.pushData(attributeNames);

            //store all fields in password_specs database (id, salt, iterations)
//            int rows = passwordDAO.pushData(id, salt, iterations);
            passwordDAO.pushData(id, salt, iterations);
        }
        rows.clear();
    }

    counterC++;
    if (status_completed) {
        System.err.println("DONE 'ADD USERS'");
        response.setHeader("REFRESH", "0");
        status_completed = false;
    }
%>
</body>
</html>

<%
        }
    } else {
        System.err.println("no one logged in, session is null");
        response.sendRedirect("/index.jsp");
    }
%>

