package servlets;

import javax.mail.Session;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Enumeration;


@WebServlet(name = "PasswordReset")
public class PasswordReset extends HttpServlet {

    @Override
    public void init() throws ServletException {
        System.err.println("init test");
    }

    public PasswordReset() {

//        String token = "";

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }


    public void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.err.println("   ***   PasswordReset servlet   ***");
        String token = request.getParameter("key");
        String login = "#";
        System.err.println("token: " + token);

        HttpSession session = request.getSession();

        //   ***   Print ALL sessions names and values list
        System.err.println("print out all the sessions");
        Enumeration attrs = session.getAttributeNames(); //you will need to include java.util.Enumeration
        while (attrs.hasMoreElements()) { //for each item in the session array
            login = attrs.nextElement().toString(); //the name of the attribute
            String sessAtr = (String) session.getAttribute(login);

            System.err.println("'" + login + "': '" + sessAtr + "'"); //print out the key/value pair

            if (sessAtr.equals(token)){
                System.err.println("   ^^^   confirmed token!");
                login = login.replace("_reset", "");  // get rid of _reset prefix
                session.setAttribute("login_psw_reset", login);  // put into a session to perform actual password reset

                response.sendRedirect("./password_reset_perform.jsp");
            }
        }
    }
}
