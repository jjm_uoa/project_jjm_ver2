package servlets;

import DAO.ArticleDAO;
import DAO.CommentsDAO;
import DAO.Models.Article;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

public class DeleteArticle extends HttpServlet {

    CommentsDAO deleteFromDataBase;
    ArticleDAO retrieve;

    @Override
    public void init() throws ServletException {
        super.init();

        String path = getServletContext().getInitParameter("sql_config");
        deleteFromDataBase = new CommentsDAO(this.getServletContext().getRealPath(path));
        retrieve = new ArticleDAO(this.getServletContext().getRealPath(path));
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        //delete the children comments
        HttpSession session = req.getSession();
        String thisArticleId = session.getAttribute("articleID").toString();
        deleteFromDataBase.deleteCommentAttachedToSpecificArticle(thisArticleId);

        //delete the parent actual article

        retrieve.deleteArticle(thisArticleId);

        //call the article page again.
        String timeStamp = new SimpleDateFormat("yyyy-MM-dd-HH:mm:ss").format(Calendar.getInstance().getTime());

        List<Article> articles = retrieve.getAllarticles(timeStamp);

        //set the attributes so can call variable in jsp

        req.setAttribute("articles", articles);

        // call jsp

        ServletContext sc = this.getServletContext();
        RequestDispatcher rd = sc.getRequestDispatcher("/articlePage.jsp");
        rd.forward(req, resp);




    }
}
