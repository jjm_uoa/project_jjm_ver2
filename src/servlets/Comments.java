package servlets;

import DAO.ArticleDAO;
import DAO.CommentsDAO;
import DAO.Models.Article;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;


public class Comments extends HttpServlet {

    CommentsDAO commentDatabaseInteraction;
    String comment;
    ArticleDAO retrieve;


    @Override
    public void init() throws ServletException {
//        super.init();
        String path = getServletContext().getInitParameter("sql_config");
        commentDatabaseInteraction = new CommentsDAO(this.getServletContext().getRealPath(path));
        retrieve = new ArticleDAO(this.getServletContext().getRealPath(path));
    }


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


        // retriveing the article id from session

        HttpSession session = request.getSession();
        String thisArtID = session.getAttribute("articleID").toString();

        Article article = retrieve.getSpecificArticleContent(thisArtID);


        //retrieving login from session
        String login = session.getAttribute("login").toString();
        comment = request.getParameter("comment");

        //gathering the userid as well as the article id from DAO
        List<String> userIdArticleId = commentDatabaseInteraction.getUserIdarticleId(login);


        String userId = userIdArticleId.get(1);

        //sending the comments with required data to database

        commentDatabaseInteraction.commentToDatabase(thisArtID, userId, comment);




        //gathering all the comments from the datatbase associated with the specific article
        List<DAO.Models.Comments> comments = commentDatabaseInteraction.getArticleComments(thisArtID);


        //passing the comment list and articles of comments to the jsp so we can use there.
        request.setAttribute("newComments", comments);
        request.setAttribute("article", article);

        //calling the jsp to print all updates.
            ServletContext sc = this.getServletContext();
        RequestDispatcher rd = sc.getRequestDispatcher("/UpdatedComments.jsp");
        rd.forward(request, response);


    }
}
