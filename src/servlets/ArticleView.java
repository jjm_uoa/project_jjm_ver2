package servlets;

import DAO.ArticleDAO;
import DAO.CommentsDAO;
import DAO.Models.Article;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

public class ArticleView extends HttpServlet{

    ArticleDAO retrieve;
    CommentsDAO commentDatabaseInteraction;
    String comment;

    @Override
    public void init() throws ServletException {





//
        String path = getServletContext().getInitParameter("sql_config");

        retrieve = new ArticleDAO(this.getServletContext().getRealPath(path));
        commentDatabaseInteraction = new CommentsDAO(this.getServletContext().getRealPath(path));
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {



        HttpSession session = request.getSession();

        //checking if user is logged in.
        if(session.getAttribute("login") != null) {

            //if logged in then get the article id from the button that I dynamically
            //assigned articleId to each button
            String thisArtID = request.getParameter("thisArticleID");

            //call the Dao using the specific article ID to retrieve that specific Article
            Article article = retrieve.getSpecificArticleContent(thisArtID);

            //create a session so we can get the article id throughout our site
            session.setAttribute("articleID", thisArtID);

            //retrieving the articleuserID specifically so we can create a session that
            //we can use later when needed. when we need to link two tables in DAO
            String articleUserID = retrieve.getArticleUserID(thisArtID);
            session.setAttribute("articleUserID", articleUserID);

            //getting the userIDfrom account

            String accountUserID = session.getAttribute("id").toString();


            //  gathering all the comments from data base associated with the article.
            List<DAO.Models.Comments> comments = commentDatabaseInteraction.getArticleComments(thisArtID);
           //setting attributes so we can use this data in jsp
            request.setAttribute("existComments", comments);
            request.setAttribute("article", article);

            //calling the jsp

            ServletContext sc = this.getServletContext();
            RequestDispatcher rd = sc.getRequestDispatcher("/articleView.jsp");
            rd.forward(request, response);
        } else {

            //same logic if not logged in


            String thisArtID = request.getParameter("thisArticleID");
            Article article = retrieve.getSpecificArticleContent(thisArtID);


            List<DAO.Models.Comments> comments = commentDatabaseInteraction.getArticleComments(thisArtID);
            request.setAttribute("existComments", comments);
            request.setAttribute("article", article);


            ServletContext sc = this.getServletContext();
            RequestDispatcher rd = sc.getRequestDispatcher("/articleView.jsp");
            rd.forward(request, response);





        }







    }
}
