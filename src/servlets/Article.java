package servlets;

import DAO.ArticleDAO;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

public class Article extends HttpServlet {

    DAO.Models.Article storage;
    ArticleDAO giveToDataBase;
    String title;
    String content;

    @Override
    public void init() throws ServletException {
        super.init();
        String path = getServletContext().getInitParameter("sql_config");
        giveToDataBase = new ArticleDAO(this.getServletContext().getRealPath(path));


    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // needed variables to store in data base
        title = req.getParameter("title");
        content = req.getParameter("content");

        // needed session login unique id that allows for query
        // that isolates the needed id number to store in
        //data base

        HttpSession session = req.getSession();

        // created time stamp for this moment to check between a users date and the date now now.
        String timeStamp = new SimpleDateFormat("yyyy-MM-dd-HH:mm:ss").format(Calendar.getInstance().getTime());

        //checking if user is logged in or not if so get the date they put in, their login
        //and their user id also and insert this data into database
        if(session.getAttribute("login") != null) {

            String date = req.getParameter("date");

            String login = session.getAttribute("login").toString();

            String user_id = String.valueOf(giveToDataBase.getUserID(login));

            //setting session to use grab id in later servlets
            session.setAttribute("userID", user_id);







           //checking that there is a title, content and that that there is a user id,
            //if there is, if user didnt add a date then we will pass in the time now, if
            //they did add a date then we pass that to to the data base.

            if(title != null && content != null && user_id != null) {
                System.out.println("");
                if(!date.isEmpty()) {
                    giveToDataBase.addToDataBase(user_id, title, content, date);

                }else{

                    giveToDataBase.addToDataBase(user_id, title, content, timeStamp);
                }
            }

            //gathering all the articles in the form of a java bean objects that
            //contains all needed columns from both account table as well as
            //article table
            //we pass in time stamp so that the dao can check if the date the user put in is
            //less than or greater than the date now. this allows for users to have articles post
            //in the future.


            List<DAO.Models.Article> articles = giveToDataBase.getAllarticles(timeStamp);

            //set the attribute so can call variable in jsp and place the data as needed.

            req.setAttribute("articles", articles);


            ServletContext sc = this.getServletContext();
            RequestDispatcher rd = sc.getRequestDispatcher("/articlePage.jsp");
            rd.forward(req, resp);


        }else{

            //if not logged in get all the articles
            //without sessions being created or called. as they will be null.

            List<DAO.Models.Article> articles = giveToDataBase.getAllarticles(timeStamp);

            //set the attributes so can call variable in jsp

            req.setAttribute("articles", articles);

            //call the srticle page jsp.


            ServletContext sc = this.getServletContext();
            RequestDispatcher rd = sc.getRequestDispatcher("/articlePage.jsp");
            rd.forward(req, resp);

        }
    }
}