package servlets;

import DAO.AccountDAO;
import DAO.ArticleDAO;
import DAO.Models.Article;
import DAO.PasswordDAO;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import static Utils.Passwords.base64Decode;
import static Utils.Passwords.isExpectedPassword;

@WebServlet(name = "Login")
public class Login extends HttpServlet {

    AccountDAO accountDAO;
    PasswordDAO passwordDAO;
    ArticleDAO giveToDataBase;


    private String login;
    private String password;
    private String hashedPassword;
    private byte[] salt;
    private int iterations;
    int id;

    String logMessage;

    @Override
    public void init() throws ServletException {
        super.init();

        String path = getServletContext().getInitParameter("sql_config");

//        accountDAO = new AccountDAO(this.getServletContext().getRealPath("WEB-INF/mysqlHome.properties"));
//        passwordDAO = new PasswordDAO(this.getServletContext().getRealPath("WEB-INF/mysqlHome.properties"));

        accountDAO = new AccountDAO(this.getServletContext().getRealPath(path));
        passwordDAO = new PasswordDAO(this.getServletContext().getRealPath(path));
        giveToDataBase = new ArticleDAO(this.getServletContext().getRealPath(path));


    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        System.err.println("   ***   Login servlet   ***");


        HttpSession session = request.getSession();
        boolean erase_mode = false;
        session.setAttribute("confirm_to_erase", "false");
        if (session.getAttribute("erase_mode").equals("true")) {
            erase_mode = true;
            session.setAttribute("attempt_to_delete_account", "true");
        }

        System.err.println("DELETE USER mode: " + erase_mode);


//        printWriter = response.getWriter();


        login = request.getParameter("username");
        password = request.getParameter("password");
        System.err.println("Entered login: " + login);
        System.err.println("Entered password: " + password);
		
		
        System.out.println("login serv called");
        String avatarPath = accountDAO.getAvatarPath(login);
        System.out.println(avatarPath);
        session.setAttribute("avatarPath", avatarPath);
		

        //check first if account is in database
        if (!accountDAO.isAccountExist(login)) {
//            printWriter.println("Account " + login + " doesn't exist");
            System.err.println("Account " + login + " doesn't exist");

            session.setAttribute("login_status", "noSuchUser");
            ServletContext sc = this.getServletContext();
//            RequestDispatcher rd = sc.getRequestDispatcher("/login_noSuchUser.jsp");
            RequestDispatcher rd = sc.getRequestDispatcher("/login.jsp");
            rd.forward(request, response);


//            session.setAttribute("isLogged", "false");
        } else {
            //get hashed password
            hashedPassword = accountDAO.getHashedPassword(login);
            System.err.println("hash: " + hashedPassword);

            //get user id
            id = accountDAO.IDbyLogin(login);

            System.err.println("id:" + id);

            byte[] hashFromString = base64Decode(hashedPassword);
            int iterations = passwordDAO.getIterationsByID(id);
            System.err.println("iterations:" + iterations);
            byte[] salt = passwordDAO.getSaltByID(id);
            System.err.println("salt size=" + salt.length);
            boolean check = isExpectedPassword(password.toCharArray(), salt, iterations, hashFromString);

            System.err.println("password is expected: " + check);


            if (check) {
                //  *** CREATE SESSION VARIABLES ***
                session.setAttribute("id", String.valueOf(id));
                session.setAttribute("login", String.valueOf(login));

                String accountRole = accountDAO.getUserRoleByID(id);
                session.setAttribute("accountRole", accountRole);

                System.err.println("SET session 'id' = " + session.getAttribute("id"));
                System.err.println("SET session 'login' = " + session.getAttribute("login"));
                System.err.println("SET session 'accountRole' = " + accountRole);


                if (erase_mode) {
                    session.setAttribute("confirm_to_erase", "true");
                    System.err.println("erase mode enabled, will redirect to deleteAccount.jsp page to decide delete and perform deletion if needed.");

                    ServletContext sc = this.getServletContext();
                    RequestDispatcher rd = sc.getRequestDispatcher("/deleteAccount.jsp");
                    rd.forward(request, response);
                }

                //  ***   Jesse logic REDIRECTION
                String timeStamp = new SimpleDateFormat("yyyy-MM-dd-HH:mm:ss").format(Calendar.getInstance().getTime());
                List<Article> articles = giveToDataBase.getAllarticles(timeStamp);
//                System.out.println(articles);
                request.setAttribute("articles", articles);
                ServletContext sc = this.getServletContext();
                RequestDispatcher rd = sc.getRequestDispatcher("/articlePage.jsp");
                rd.forward(request, response);
            } else {
                session.setAttribute("login_status", "wrongPassword");
                ServletContext sc = this.getServletContext();
                RequestDispatcher rd = sc.getRequestDispatcher("/login.jsp");
                rd.forward(request, response);
            }
        }
        accountDAO.close();
        System.err.println("-----------------------------------------------------------------------------------");
    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
