package servlets;

import DAO.AccountDAO;
import DAO.PasswordDAO;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.imageio.ImageIO;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import java.util.List;

import static Utils.Passwords.*;

@WebServlet(name = "SignUp")
public class SignUp extends HttpServlet {


    Map<String, String> attributeNames;
    AccountDAO accountDAO;
    PasswordDAO passwordDAO;
    byte[] salt;
    int iterations;
    private boolean isUploadedAvatar;

    @Override
    public void init() throws ServletException {
        super.init();
        attributeNames = new HashMap<>();
        String path = getServletContext().getInitParameter("sql_config");
        accountDAO = new AccountDAO(this.getServletContext().getRealPath(path));
        passwordDAO = new PasswordDAO(this.getServletContext().getRealPath(path));
    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {



        System.err.print("   ***   SignUp servlet   ***\nEntered data: ");


        ServletContext context = request.getServletContext();
        HttpSession session = request.getSession();

        session.removeAttribute("id");

        DiskFileItemFactory factory = new DiskFileItemFactory();
        String web_path_to_avatars = "/avatars/";
        String real_path_to_avatars = context.getRealPath(web_path_to_avatars);
        factory.setRepository(new File(real_path_to_avatars));
        ServletFileUpload upload = new ServletFileUpload(factory);
        upload.setSizeMax(5242880L);
        String fieldName;
        String fileName = "";
        long sizeInBytes;
        attributeNames.put("file", "");
        attributeNames.put("system_avatars", "");
        FileItem fi = null;


        try {
            List fileItems = upload.parseRequest(request);
            Iterator iter = fileItems.iterator();
            do {


                fi = (FileItem) iter.next();

                if (fi.isFormField()) {

                    fieldName = fi.getFieldName();

                    if (fieldName.equals("username"))
                        attributeNames.put("username", fi.getString());

                    if (fieldName.equals("firstname"))
                        attributeNames.put("firstname", fi.getString());

                    if (fieldName.equals("lastname"))
                        attributeNames.put("lastname", fi.getString());

                    if (fieldName.equals("middlename"))
                        attributeNames.put("middlename", fi.getString());

                    if (fieldName.equals("password_hash"))
                        attributeNames.put("password_hash", fi.getString());

                    if (fieldName.equals("email"))
                        attributeNames.put("email", fi.getString());

                    if (fieldName.equals("birthday"))
                        attributeNames.put("birthday", fi.getString());

                    if (fieldName.equals("country"))
                        attributeNames.put("country", fi.getString());

                    if (fieldName.equals("file"))
                        attributeNames.put("file", fi.getString());

                    if (fieldName.equals("description"))
                        attributeNames.put("description", fi.getString());

                    if (fieldName.equals("password"))
                        attributeNames.put("password", fi.getString());

                    if (fieldName.equals("password_confirm"))
                        attributeNames.put("password_confirm", fi.getString());

                    if (fieldName.equals("system_avatars"))
                        attributeNames.put("system_avatars", fi.getString());

//                    System.out.println();

                } else {
                    fileName = fi.getName();
                    attributeNames.put("file", fileName);
                    System.err.println("custom avatar fileName: " + fileName);

                    if (fileName != "") {

                        //WRITE to file

//                        String finalName = attributeNames.get("file") + session.getId() + fileName.substring(fileName.lastIndexOf("."));

                        String avatar_name = attributeNames.get("file");
                        System.out.println("avatar_name:" + avatar_name);
                        String extension = avatar_name.substring(avatar_name.lastIndexOf("."));
                        System.out.println("extension:" + extension);
                        String fileNoExtension = avatar_name.substring(0, avatar_name.lastIndexOf("."));
                        System.out.println("fileNoExt" + fileNoExtension);
                        System.out.println("PATH:" + real_path_to_avatars);


                        File file = new File(real_path_to_avatars + avatar_name);
//                        String avatarPath = finalName;
                        try {
                            fi.write(file);  // upload the original image
                            System.err.println("The file: " + avatar_name + " uploaded to fullpath directory: " + real_path_to_avatars);


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        BufferedImage img = null;

                        try {
                            img = ImageIO.read(new File(real_path_to_avatars + avatar_name)); //read uploaded avatar
                            String thumbnailFile = avatar_name.replace(avatar_name.substring(avatar_name.lastIndexOf(".")), "_thumbnail.jpg");
                            System.err.println("thumbnail name: " + thumbnailFile);
                            if (img.getHeight() < 300 && img.getWidth() < 300) {
                                ImageIO.write(img, "jpg", new File(real_path_to_avatars + thumbnailFile));
                            } else {
                                double zoom = Math.max(1.0D * (double) img.getHeight() / 100.0D, 1.0D * (double) img.getWidth() / 100.0D);
                                int type = img.getType() == 0 ? 2 : img.getType();
                                BufferedImage resizedImage = new BufferedImage((int) ((double) img.getWidth() / zoom), (int) ((double) img.getHeight() / zoom), type);
                                Graphics2D g = resizedImage.createGraphics();
                                g.drawImage(img, 0, 0, (int) ((double) img.getWidth() / zoom), (int) ((double) img.getHeight() / zoom), (ImageObserver) null);
                                g.dispose();
                                g.setComposite(AlphaComposite.Src);
                                g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
                                g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
                                g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
                                ImageIO.write(resizedImage, "jpg", new File(real_path_to_avatars + thumbnailFile));
                            }

                            isUploadedAvatar = true;
                            attributeNames.put("file", web_path_to_avatars + thumbnailFile);

                            //delete original file
                            file = new File(real_path_to_avatars + avatar_name);
                            file.delete();

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            while (iter.hasNext());

        } catch (FileUploadException e) {
            e.printStackTrace();
        }

        //form validation block for checking things like an empty fields or password doesn't match
        //check them and if OK, proceed them to database otherwise, do something, for now - it is a simple message to printWriter
        PrintWriter printWriter = response.getWriter();

        String logMessage = "";


        if (attributeNames.get("username").equals("")) logMessage += "Empty userName\n";
        if (attributeNames.get("firstname").equals("")) logMessage += "Empty First Name\n";
        if (attributeNames.get("lastname").equals("")) logMessage += "Empty Last Name\n";
        if (attributeNames.get("middlename").equals("")) logMessage += "Empty Middle Name\n";
        if (attributeNames.get("email").equals("")) logMessage += "Empty E-mail\n";
        if (attributeNames.get("description").equals("")) logMessage += "Empty description\n";
        if (attributeNames.get("password").equals("") || attributeNames.get("password_confirm").equals(""))
            logMessage += "Empty password\n";
        if ((attributeNames.get("password").equals(attributeNames.get("password_confirm")))
                && (attributeNames.get("password").equals("")))
            logMessage += "Passwords doesn't match";


        //additional logic to check input fields
        String ps = attributeNames.get("password");

        //check password strength [4 at least, upper and lower case, 1 number\or special character]
        if (ps.length() < 4) logMessage += "Your password is less than 4 characters\n";
        if (ps.equals(ps.toLowerCase())) logMessage += "Your password must contain at least 1 upper character\n";
        if (ps.equals(ps.toUpperCase())) logMessage += "Your password must contain at least 1 lower character\n";
        ps = ps.toLowerCase();
        String remover = "abcdefghijklmnopqrstuvwxyz";
        for (int i = 0; i < remover.length(); i++) {
            ps = ps.replace("" + remover.charAt(i), "");
        }
        if (ps.length() == 0) logMessage += "Your password must contain at least 1 number or special character\n";


        if (!logMessage.equals("")) {
            logMessage = "Incorrect data entered: \n\n" + logMessage;
            printWriter.println(logMessage);
        } else {

            //if no custom avatar is provided, generate the file name
            //what is th number of the avatar?

            if (attributeNames.get("file").equals("") || !isUploadedAvatar) {
                int avatarNum = 0;
                if (!attributeNames.get("system_avatars").equals("")) {
                    avatarNum = Integer.parseInt(attributeNames.get("system_avatars").replace("avatar", ""));
                    attributeNames.put("file", web_path_to_avatars + "avatar" + avatarNum + ".png");
                } else
                    attributeNames.put("file", web_path_to_avatars + "avatar1.png");
            }
        }


        attributeNames.put("password_hash", calculateHash());

        //store all fields in account database
        int id = accountDAO.pushData(attributeNames);

        //store all fields in password_specs database (id, salt, iterations)
        int rows = passwordDAO.pushData(id, salt, iterations);


        session.setAttribute("userCreated" , "success");

        if (rows != 0) printWriter.println("New user " + attributeNames.get("username") + " with id# " + id + " successfully created.");
        else {
            printWriter.println("Error while creating a new user");
            session.setAttribute("userCreated" , "fail");
        }


        response.sendRedirect("/login.jsp");
    }


    protected String calculateHash() {

        Random random = new Random();

        //random number of iterations [32..64]
        iterations = random.nextInt(33) + 32;
        salt = getNextSalt(iterations);

//        String saltString = base64Encode(salt);
        String password = attributeNames.get("password");
        char[] charArray = password.toCharArray();
        byte[] hashedPassword = hash(charArray, salt, iterations);
        String passwordHashedString = base64Encode(hashedPassword);

        System.err.println("salt length: " + salt.length);
        System.err.println("not processed password: " + password);
//        System.err.println("hashed password length: " + hashedPassword.length);
        System.err.println("hashed password string: " + passwordHashedString);

        return passwordHashedString;
    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws javax.servlet.ServletException, IOException {

        doPost(request, response);

    }

}
