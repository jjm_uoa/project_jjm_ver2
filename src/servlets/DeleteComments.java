package servlets;

import DAO.ArticleDAO;
import DAO.CommentsDAO;
import DAO.Models.Article;
import DAO.Models.Comments;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

public class DeleteComments extends HttpServlet {

    CommentsDAO deleteFromDataBase;
    ArticleDAO retrieve;

    @Override
    public void init() throws ServletException {
        super.init();
        String path = getServletContext().getInitParameter("sql_config");
        deleteFromDataBase = new CommentsDAO(this.getServletContext().getRealPath(path));
        retrieve = new ArticleDAO(this.getServletContext().getRealPath(path));


    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

       //deleting article from database
        String commentId = req.getParameter("thisCommentID");
        System.out.println(commentId);
        deleteFromDataBase.deleteComment(commentId);

        //refreshing the page with article and deleted comment

        HttpSession session = req.getSession();



        String sessionThisID = session.getAttribute("articleID").toString();



        List<Comments> comments = deleteFromDataBase.getArticleComments(sessionThisID);
        req.setAttribute("newComments",comments);
        Article article = retrieve.getSpecificArticleContent(sessionThisID);
        req.setAttribute("article", article);


        ServletContext sc = this.getServletContext();
        RequestDispatcher rd = sc.getRequestDispatcher("/UpdatedComments.jsp");
        rd.forward(req, resp);




    }
}
