package servlets;

import DAO.ArticleDAO;
import DAO.Models.Article;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

public class UserWantsOwnArticles extends HttpServlet{

    ArticleDAO giveToDataBase;

    @Override
    public void init() throws ServletException {
        String path = getServletContext().getInitParameter("sql_config");
        giveToDataBase = new ArticleDAO(this.getServletContext().getRealPath(path));

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        String login = session.getAttribute("login").toString();

        List<Article> articles =  giveToDataBase.getUserOwnArticles(login);

        req.setAttribute("articles", articles);


        ServletContext sc = this.getServletContext();
        RequestDispatcher rd = sc.getRequestDispatcher("/articlePage.jsp");
        rd.forward(req, resp);

    }
}
