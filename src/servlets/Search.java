package servlets;

import DAO.ArticleDAO;
import DAO.Models.Article;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class Search extends HttpServlet{

    ArticleDAO search;

    @Override
    public void init() throws ServletException {
        String path = getServletContext().getInitParameter("sql_config");
        search = new ArticleDAO(this.getServletContext().getRealPath(path));



    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        String searchCritera = req.getParameter("searchCriteria");
        String searchBox = req.getParameter("searchBox");
        System.out.println(searchBox);
        System.out.println(searchCritera);

        if (searchCritera.equals("title")) {

            List<Article> searchedTitleArticles = search.searchArticlesbyTitle(searchBox);
            if (searchedTitleArticles.size() == 0) {
                String message = (" Sorry there were no articles that had the title: " + searchBox);
                req.setAttribute("titleMessage", message);

            }
            req.setAttribute("articles", searchedTitleArticles);


            ServletContext sc = this.getServletContext();
            RequestDispatcher rd = sc.getRequestDispatcher("/articlePage.jsp");
            rd.forward(req, resp);

        } else if (searchCritera.equals("user_id")) {
            List<Article> searchedUserNameArticles = search.searchArticlesbyUserName(searchBox);
            if (searchedUserNameArticles.size() == 0) {
                String message = (" Sorry there were no articles that had the user name of: " + searchBox);
                req.setAttribute("titleMessage", message);

            }
            req.setAttribute("articles", searchedUserNameArticles);


            ServletContext sc = this.getServletContext();
            RequestDispatcher rd = sc.getRequestDispatcher("/articlePage.jsp");
            rd.forward(req, resp);


        } else if (searchCritera.equals("created_time")) {
            List<Article> searchedbydateArticles = search.searchArticlesbyDate(searchBox);
            if (searchedbydateArticles.size() == 0) {
                String message = (" Sorry there were no articles that had the date of: " + searchBox);
                req.setAttribute("titleMessage", message);
            }

            req.setAttribute("articles", searchedbydateArticles);


            ServletContext sc = this.getServletContext();
            RequestDispatcher rd = sc.getRequestDispatcher("/articlePage.jsp");
            rd.forward(req, resp);
        }
    }
}
