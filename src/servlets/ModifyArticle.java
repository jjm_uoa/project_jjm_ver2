package servlets;

import DAO.ArticleDAO;
import DAO.CommentsDAO;
import DAO.Models.Article;
import DAO.Models.Comments;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

public class ModifyArticle extends HttpServlet {

    ArticleDAO makeQuery;
    CommentsDAO commentDatabaseInteraction;



    @Override
    public void init() throws ServletException {
        super.init();
        String path = getServletContext().getInitParameter("sql_config");
        makeQuery = new ArticleDAO(this.getServletContext().getRealPath(path));
        commentDatabaseInteraction = new CommentsDAO(this.getServletContext().getRealPath(path));
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        String modifiedTitle = req.getParameter("titleChange");
        String modifiedContent= req.getParameter("contentChange");
        HttpSession session = req.getSession();
        String articleID = session.getAttribute("articleID").toString();


        //putting the modifications to articles into the data base

        makeQuery.modifyContentandTitle(articleID,modifiedTitle,modifiedContent);

        //getting them out

        String sessionThisID = session.getAttribute("articleID").toString();
        List<Comments> comments = commentDatabaseInteraction.getArticleComments(sessionThisID);
        req.setAttribute("newComments",comments);
        Article article = makeQuery.getSpecificArticleContent(sessionThisID);
        req.setAttribute("article", article);

        //calling jsp
        ServletContext sc = this.getServletContext();
        RequestDispatcher rd = sc.getRequestDispatcher("/UpdatedComments.jsp");
        rd.forward(req, resp);



    }
}
