package DAO;

import DAO.Models.Comments;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CommentsDAO implements AutoCloseable {

    Connection connector;
    String query;
    ConnectionMySQL mySql;

//    Account account;

    public CommentsDAO(String config) {
        connector = null;
        query = null;
        mySql = new ConnectionMySQL(config);
//        account = new Account();
    }


    // this method will retrieve all comments associated with a particular article
    // where the comments articleId (fk) = the actual articleId

    public List<Comments> getArticleComments(String articleID) {

        List<Comments> articleCommentList = new ArrayList<>();
        connector = mySql.establish();

        query = "SELECT DISTINCT c.comment_id, c.article_id, c.user_id, c.comment_content, c.created_time, c.modified_time, acc.login  FROM comments AS c, article AS ar, account as acc WHERE acc.id = c.user_id and c.article_id= ? and c.hidden = 0 ORDER BY c.created_time DESC;";
//        query = "SELECT * from comments";
        try {
            PreparedStatement prep = connector.prepareStatement(query);
            prep.setString(1, articleID);

            try (ResultSet results = prep.executeQuery()) {

                while (results.next()) {

                    String commentId = results.getString(1);
                    String articleId = results.getString(2);
                    String userId = results.getString(3);
                    String commentContent = results.getString(4);
                    String cTime = results.getString(5);
                    String mTime = results.getString(6);
                    String login = results.getString(7);

                    Comments commentobj = new Comments(commentId, articleId, userId, commentContent, cTime, mTime, login);
                    articleCommentList.add(commentobj);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        System.out.println("teting this bit");
        return articleCommentList;
    }


    public List<String> getUserIdarticleId(String login) {

        List<String> userAndArticle = new ArrayList<>();
        String artcileIdTemp = "";
        String idTemp = "";

        connector = mySql.establish();
        query = "SELECT article_id, id FROM article AS ar, account AS ac WHERE ar.user_id = ac.id AND ac.login = ? ;";
        try {
            PreparedStatement prep = connector.prepareStatement(query);
            prep.setString(1, login);

            try (ResultSet result = prep.executeQuery()) {

                while (result.next()) {

                    String articleId = result.getString(1);
                    String id = result.getString(2);

                    artcileIdTemp = articleId;
                    idTemp = id;

                }
                userAndArticle.add(artcileIdTemp);
                userAndArticle.add(idTemp);

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return userAndArticle;
    }

    public void commentToDatabase(String articleId, String userId, String comment) {
        connector = mySql.establish();
        query = "insert into comments (article_id, user_id, comment_content) VALUES (?, ?, ?)";

        try {
            PreparedStatement prep = connector.prepareStatement(query);

            prep.setString(1, articleId);
            prep.setString(2, userId);
            prep.setString(3, comment);
            prep.executeUpdate();

            System.out.println("reached");

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void deleteComment(String commentID) {
        connector = mySql.establish();
        query = "DELETE FROM comments\n" +
                "WHERE comment_id = ?;";
        System.out.println("called");


        try {
            PreparedStatement prep = connector.prepareStatement(query);
            prep.setString(1, commentID);
            prep.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteCommentAttachedToSpecificArticle(String articleid) {
        connector = mySql.establish();
        query = "DELETE FROM comments\n" +
                "WHERE article_id = ?;";


        try {
            PreparedStatement prep = connector.prepareStatement(query);
            prep.setString(1, articleid);
            prep.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public String getCommentUserID(String commentID) {
        String theCommentsUserID = "";
        connector = mySql.establish();
        query = "SELECT user_id \n" +
                "FROM comments\n" +
                "WHERE comment_id = ? ;";

        try {
            PreparedStatement pst = connector.prepareStatement(query);
            pst.setString(1, commentID);

            try (ResultSet r = pst.executeQuery()) {
                while (r.next()) {
                    String commentsUserid = r.getString("user_id");
                    theCommentsUserID = commentsUserid;

                }

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return theCommentsUserID;

    }


    @Override
    public void close() throws Exception {
        mySql.close();
    }
}
