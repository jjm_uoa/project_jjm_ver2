package DAO;

import DAO.Models.Article;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ArticleDAO implements AutoCloseable {

    Connection connector;
    ConnectionMySQL mySQL;
    String query;


    public ArticleDAO(String config) {
        connector = null;
        mySQL = new ConnectionMySQL(config);
        query = null;


    }


    public List<Article> getAllarticles(String currentTime) {
        List<Article> articles = new ArrayList<>();
        connector = mySQL.establish();
        query = "SELECT article_id, firstname, lastname, user_id, title, content,created_time,modified_time FROM article AS a, account AS ac WHERE a.user_id = ac.id AND a.hidden = 0 and ? >= a.UserDate ORDER BY a.created_time DESC ";
        try {
            PreparedStatement pst = connector.prepareStatement(query);
            pst.setString(1, currentTime);

            try (ResultSet r = pst.executeQuery()) {

                extractData(articles, r);

            }


            pst.close();

        } catch (SQLException e) {
            e.printStackTrace();


        }
        return articles;

    }

    public List<Article> getUserOwnArticles(String loginid) {
        List<Article> userArticles = new ArrayList<>();
        connector = mySQL.establish();
        query = "SELECT article_id, firstname, lastname, user_id, title, content,created_time,modified_time FROM article AS a, account AS ac WHERE a.user_id = ac.id  AND ac.login = ? ORDER BY a.created_time DESC ";

        try {
            PreparedStatement pst = connector.prepareStatement(query);
            pst.setString(1, loginid);

            try (ResultSet r = pst.executeQuery()) {

                extractData(userArticles, r);

            }


        } catch (SQLException e) {
            e.printStackTrace();


        }
        return userArticles;

    }


    public boolean addToDataBase(String id, String title, String content, String userTime) {
        boolean returnValue = false;

        connector = mySQL.establish();
        query = "INSERT INTO article (user_id, title, content,UserDate) VALUES (?,?,?,?);";

        try {
            PreparedStatement pst = connector.prepareStatement(query);
            pst.setString(1, id);
            pst.setString(2, title);
            pst.setString(3, content);
            pst.setString(4, userTime);
            int i = pst.executeUpdate();
            System.err.println(i + " row(s) affected in account");
            if (i > 0)
                returnValue = true;
        } catch (SQLException e) {
            System.err.println("Error while adding an article");
            e.printStackTrace();
        } finally {
            try {
                close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return returnValue;
    }

    public String getUserID(String login) {
        String theUserId = "";
        connector = mySQL.establish();
        query = "SELECT id\n" +
                "FROM account\n" +
                "WHERE login = ? ;";

        try {
            PreparedStatement pst = connector.prepareStatement(query);
            pst.setString(1, login);

            try (ResultSet r = pst.executeQuery()) {
                while (r.next()) {
                    String user_id = r.getString("id");
                    theUserId = user_id;

                }

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return theUserId;

    }

    public String getArticleUserID(String articleID) {
        String theArticleID = "";
        connector = mySQL.establish();
        query = "SELECT user_id \n" +
                "FROM article\n" +
                "WHERE article_id = ? ;";

        theArticleID = tryingCatching(articleID, theArticleID);
        return theArticleID;

    }

    private String tryingCatching(String articleID, String theArticleID) {
        try {
            PreparedStatement pst = connector.prepareStatement(query);
            pst.setString(1, articleID);

            try (ResultSet r = pst.executeQuery()) {
                while (r.next()) {
                    String articleUserid = r.getString("user_id");
                    theArticleID = articleUserid;

                }

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return theArticleID;
    }


    //retrieves one specific article needed when users press button to read more

    public Article getSpecificArticleContent(String articleID) {

        connector = mySQL.establish();
        query = "SELECT article_id, firstname, lastname, user_id, title, content,created_time,modified_time FROM article AS a, account AS ac WHERE a.user_id = ac.id  AND a.article_id = ? ";

        try {
            PreparedStatement pst = connector.prepareStatement(query);
            pst.setString(1, articleID);

            try (ResultSet r = pst.executeQuery()) {

                while (r.next()) {

                    String articalid = r.getString(1);
                    String firstname = r.getString(2);
                    String lastname = r.getString(3);
                    String userid = r.getString(4);
                    String title = r.getString(5);
                    String content = r.getString(6);
                    String createdtime = r.getString(7);
                    String modifiedTime = r.getString(8);


                    Article theArticle = new Article(articalid, firstname, lastname, userid, title, content, createdtime, modifiedTime);


                    return theArticle;


                }

            }


        } catch (SQLException e) {
            e.printStackTrace();


        }


        return null;
    }

    public void modifyContentandTitle(String articleid, String newTitle, String newContent) {

        connector = mySQL.establish();

        query = "UPDATE article\n" +
                "SET title = ?, content = ? where article_id = ?";

        try {
            PreparedStatement pst = connector.prepareStatement(query);
            pst.setString(1, newTitle);
            pst.setString(2, newContent);
            pst.setString(3, articleid);
            pst.executeUpdate();


        } catch (SQLException e) {
            e.printStackTrace();

        }


    }

    public void deleteArticle(String articleid) {
        connector = mySQL.establish();
        query = "DELETE FROM article\n" +
                "WHERE article_id = ?;";


        catches(articleid);


    }

    private void catches(String articleid) {
        try {
            PreparedStatement prep = connector.prepareStatement(query);

            prep.setString(1, articleid);

            prep.executeUpdate();


        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<Article> searchArticlesbyTitle(String searchInput) {
        List<Article> articles = new ArrayList<>();
        connector = mySQL.establish();
        query = " SELECT article_id, firstname, lastname, user_id, title , content,created_time,modified_time FROM article AS a, account AS ac WHERE a.user_id = ac.id AND a.hidden = 0 and title like ?";
        settingsVariables(searchInput, articles);
        return articles;
    }

    private void settingsVariables(String searchInput, List<Article> articles) {
        try {
            PreparedStatement pst = connector.prepareStatement(query);


            pst.setString(1, "%" + searchInput + "%");

            try (ResultSet r = pst.executeQuery()) {

                extractData(articles, r);
            }
            pst.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void extractData(List<Article> articles, ResultSet r) throws SQLException {
        while (r.next()) {
            String articalid = r.getString(1);
            String firstname = r.getString(2);
            String lastname = r.getString(3);
            String userid = r.getString(4);
            String title = r.getString(5);
            String content = r.getString(6);
            String createdtime = r.getString(7);
            String modifiedTime = r.getString(8);
            Article toAdd = new Article(articalid, firstname, lastname, userid, title, content, createdtime, modifiedTime);
            articles.add(toAdd);
        }
    }

    public List<Article> searchArticlesbyUserName(String searchInput) {
        List<Article> articles = new ArrayList<>();
        connector = mySQL.establish();
        query = " SELECT article_id, firstname, lastname, user_id, title , content,created_time,modified_time FROM article AS a, account AS ac WHERE a.user_id = ac.id AND a.hidden = 0 and ac.login like ?";
        settingsVariables(searchInput, articles);
        return articles;

    }

    public List<Article> searchArticlesbyDate(String searchInput) {
        List<Article> articles = new ArrayList<>();
        connector = mySQL.establish();
        query = " SELECT article_id, firstname, lastname, user_id, title , content,created_time,modified_time FROM article AS a, account AS ac WHERE a.user_id = ac.id AND a.hidden = 0 and a.created_time like ?";
        settingsVariables(searchInput, articles);
        return articles;

    }

    public void dateChecker(String date) {
        connector = mySQL.establish();

        query = "UPDATE mtro205.article SET hidden = 1 WHERE  ? >= article.modified_time";


        catches(date);


    }


    @Override
    public void close() throws Exception {
        mySQL.close();

    }
}
