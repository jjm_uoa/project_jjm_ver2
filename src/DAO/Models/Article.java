package DAO.Models;

import java.io.Serializable;

public class Article implements Serializable {

    String articleID;
    String fname;
    String lname;
    String title;
    String content;
    String time;
    String mtime;
    String userID;

    public Article() {

    }


    public Article(String articleID, String author_fname, String author_lname, String userID, String title, String content, String created_time, String modified_time) {

        this.fname = author_fname;
        this.lname = author_lname;
        this.title = title;
        this.content = content;
        this.time = created_time;
        this.userID = userID;
        this.mtime = modified_time;
        this.articleID = articleID;


    }

    public String getArticleID() {
        return articleID;
    }

    public void setArticleID(String articleID) {
        this.articleID = articleID;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getMtime() {
        return mtime;
    }

    public void setMtime(String mtime) {
        this.mtime = mtime;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    @Override
    public String toString() {
        return "Article{" +
                "articleID='" + articleID + '\'' +
                ", fname='" + fname + '\'' +
                ", lname='" + lname + '\'' +
                ", title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", time='" + time + '\'' +
                ", mtime='" + mtime + '\'' +
                ", userID='" + userID + '\'' +
                '}';
    }
}