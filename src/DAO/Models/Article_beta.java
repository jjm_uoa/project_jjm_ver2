package DAO.Models;

import java.sql.Timestamp;

public class Article_beta {

        private int article_id;
        private int user_id;
        private String title;
        private String content;
        private Timestamp created_time;
        private Timestamp modified_time;
        private String authorName;

    public Article_beta(int article_id, int user_id, String title, String content, Timestamp created_time, Timestamp modified_time, String authorName) {
        this.article_id = article_id;
        this.user_id = user_id;
        this.title = title;
        this.content = content;
        this.created_time = created_time;
        this.modified_time = modified_time;
        this.authorName = authorName;
    }

    public Article_beta() {

    }

    public void setArticle_id(int article_id) {
        this.article_id = article_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setCreated_time(Timestamp created_time) {
        this.created_time = created_time;
    }

    public void setModified_time(Timestamp modified_time) {
        this.modified_time = modified_time;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public int getArticle_id() {
        return article_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public Timestamp getCreated_time() {
        return created_time;
    }

    public Timestamp getModified_time() {
        return modified_time;
    }

    public String getAuthorName() {
        return authorName;
    }
}
