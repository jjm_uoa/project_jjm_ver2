package DAO.Models;

import java.io.Serializable;

public class Comments implements Serializable{

    // comments table
    public String commmentId;
    public String articleId;
    public String userId;
    public String comment;
    public String commentCreated;
    public String commentModified;
    public String userName;


    //  foreign keys
    public String foreignArticleId;
    public String foreignUserId;

    public Comments(String commentId, String articleId, String userId, String commentContent, String cTime, String mTime, String userName) {
        this.commmentId = commentId;
        this.articleId = articleId;
        this.userId = userId;
        this.comment = commentContent;
        this.commentCreated = cTime;
        this.commentModified = mTime;
        this.userName = userName;

    }

    public String getCommmentId() {
        return commmentId;
    }

    public void setCommmentId(String commmentId) {
        this.commmentId = commmentId;
    }

    public String getArticleId() {
        return articleId;
    }

    public void setArticleId(String articleId) {
        this.articleId = articleId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String commentContent) {
        this.comment = commentContent;
    }

    public String getCommentCreated() {
        return commentCreated;
    }

    public void setCommentCreated(String commentCreated) {
        this.commentCreated = commentCreated;
    }

    public String getCommentModified() {
        return commentModified;
    }

    public void setCommentModified(String commentModified) {
        this.commentModified = commentModified;
    }

    public String getForeignArticleId() {
        return foreignArticleId;
    }

    public void setForeignArticleId(String foreignArticleId) {
        this.foreignArticleId = foreignArticleId;
    }

    public String getForeignUserId() {
        return foreignUserId;
    }

    public void setForeignUserId(String foreignUserId) {
        this.foreignUserId = foreignUserId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public String toString() {
        return "Comments{" +
                "commmentId='" + commmentId + '\'' +
                ", articleId='" + articleId + '\'' +
                ", userId='" + userId + '\'' +
                ", commentContent='" + comment + '\'' +
                ", commentCreated='" + commentCreated + '\'' +
                ", commentModified='" + commentModified + '\'' +
                ", foreignArticleId='" + foreignArticleId + '\'' +
                ", foreignUserId='" + foreignUserId + '\'' +
                '}';
    }
}
