package DAO.Models;

public class Password {

    private int userId;
    private byte[] salt;
    private int iterations;


    public Password() {
    }

    public Password(int userId, byte[] salt, int iterations) {
        this.userId = userId;
        this.salt = salt;
        this.iterations = iterations;
    }

    public int getUserId() {
        return userId;
    }

    public byte[] getSalt() {
        return salt;
    }

    public int getIterations() {
        return iterations;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public void setSalt(byte[] salt) {
        this.salt = salt;
    }

    public void setIterations(int iterations) {
        this.iterations = iterations;
    }
}
