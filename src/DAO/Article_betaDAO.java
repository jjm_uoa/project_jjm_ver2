package DAO;

import DAO.Models.Article_beta;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Article_betaDAO implements AutoCloseable {

    Connection connector;
    String query;
    ConnectionMySQL mySQL;
    Article_beta article_beta;


    public Article_betaDAO(String config) {
        connector = null;
        query = null;
        mySQL = new ConnectionMySQL(config);
        article_beta = new Article_beta();
    }

    // returns false - if article not added
    // method will add a new article what ever if the value with new auto incremented
    // article id unless the user_id is valid (presented in account) database


    //get all articles in HashMap format -> will fetch all data,
    // should be used to fetch any particular data like only titles / articles / etc ...

    public List<HashMap> getAllArticles() {
        connector = mySQL.establish();
        query = "select usr.id as ID, usr.login AS LOGIN, a.article_id AS ARTICLE_ID, a.title AS TITLE, a.content AS CONTENT, a.hidden AS HIDDEN from article as a, account as usr where a.user_id = usr.id";
//        query = "select title, content, login, created_time, modified_time  from article acc, account usr where acc.user_id =usr. id";
        ResultSet resultSet;

        HashMap<String, String> article;
        // for each article
        List<HashMap> articles_list = new ArrayList<>(); // can be one or more articles, so every article will go to separate container
        try {
            PreparedStatement pst = connector.prepareStatement(query);
            resultSet = pst.executeQuery();
            resultSet.next();

            do {
                article = new HashMap<>(); // new object for each cycle
                article.put("id", resultSet.getString(1));
                article.put("login", resultSet.getString(2));
                article.put("art_id", resultSet.getString(3));
                article.put("title", resultSet.getString(4));
                article.put("content", resultSet.getString(5));
                article.put("hidden", resultSet.getString(6));
                articles_list.add(article);
            }
            while (resultSet.next());

            resultSet.close();
            pst.close();
        } catch (SQLException e) {
            System.err.println("Error while reading data");
            e.printStackTrace();
        } finally {
            close();
        }
        return articles_list;
    }


    public void updateArticlesStatus(List<Integer> list_art) {

        System.err.println("set defaults hide status for articles -> 0");
        connector = mySQL.establish();
        query = "update article set hidden = default";

        try (PreparedStatement pst = connector.prepareStatement(query)) {
            int i = pst.executeUpdate();
//            pst.close();
            System.err.println("row(s) " + i + " affected.");

        } catch (SQLException e) {
            e.printStackTrace();
        }


//        System.err.println("updating status of articles according supplied list of ID");
        System.err.println("updates status of articles depends on provided List<Integer>");
        int counter = 0;
        for (int i = 0; i < list_art.size(); i++) {
            query = "update article set hidden = 1 where article_id = ?";

            try (PreparedStatement pst = connector.prepareStatement(query)) {
                pst.setString(1, String.valueOf(list_art.get(i)));
                counter += pst.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
            }
        }
        mySQL.close();
        System.err.println("row(s) " + counter + " affected.");

    }


    public void updateArticlesComments(List<Integer> list_comments) {
        System.err.println("set defaults hide status for comments -> 0");
        connector = mySQL.establish();
        query = "update comments set hidden = default";

        try (PreparedStatement pst = connector.prepareStatement(query)) {
            int i = pst.executeUpdate();
            System.err.println("row(s) " + i + " affected.");
        } catch (SQLException e) {
            e.printStackTrace();
        }

        System.err.println("updating status of comments depends on provided List<Integer>");
        int counter = 0;
        for (int i = 0; i < list_comments.size(); i++) {
            query = "update comments set hidden = 1 where comment_id = ?";
            try (PreparedStatement pst = connector.prepareStatement(query)) {
                pst.setString(1, String.valueOf(list_comments.get(i)));
                counter += pst.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
            }
        }
        mySQL.close();
        System.err.println("row(s) " + counter + " affected.");

    }


    public List<HashMap> getAllComments() {
        connector = mySQL.establish();
        query = "select c.comment_id, c.article_id, c.user_id, a.login, c.comment_content, c.hidden from comments AS c, account AS a where c.user_id = a.id";
//        query = "select title, content, login, created_time, modified_time  from article acc, account usr where acc.user_id =usr. id";
        ResultSet resultSet;

        HashMap<String, String> article;
        // for each article
        List<HashMap> articles_list = new ArrayList<>(); // can be one or more articles, so every article will go to separate container
        try {
            PreparedStatement pst = connector.prepareStatement(query);
            resultSet = pst.executeQuery();
            resultSet.next();

            do {
                article = new HashMap<>(); // new object for each cycle
                article.put("comment_id", resultSet.getString(1));
                article.put("art_id", resultSet.getString(2));
                article.put("id", resultSet.getString(3));
                article.put("login", resultSet.getString(4));
                article.put("content", resultSet.getString(5));
                article.put("hidden", resultSet.getString(6));
                articles_list.add(article);
            }
            while (resultSet.next());

            resultSet.close();
            pst.close();
        } catch (SQLException e) {
            System.err.println("Error while reading data");
            e.printStackTrace();
        } finally {
            close();
        }
        return articles_list;
    }


//    public boolean addArticle(int user_id, String title, String content) {
//        boolean returnValue = false;
//        connector = mySQL.establish();
//        query = "insert into article (user_id, title, content) values (?,?,?);";
//
//        try {
//            PreparedStatement pst = connector.prepareStatement(query);
//            pst.setString(1, String.valueOf(user_id));
//            pst.setString(2, String.valueOf(title));
//            pst.setString(3, String.valueOf(content));
//            int i = pst.executeUpdate();
//            System.err.println(i + " row(s) affected in account");
//            if (i > 0)
//                returnValue = true;
//        } catch (SQLException e) {
//            System.err.println("Error while adding an article");
//            e.printStackTrace();
//        } finally {
//            close();
//        }
//        return returnValue;
//    }
//
//
//    // update an article by arttilce ID
//    // will update the content
//    public void updateArticle(int article_id, String content) {
//        connector = mySQL.establish();
//        query = "update article set content = ? where article_id = ?";
//        try {
//            PreparedStatement pst = connector.prepareStatement(query);
//            pst.setString(2, String.valueOf(article_id));
//            pst.setString(1, String.valueOf(content));
//            System.err.println("row(s) " + pst.executeUpdate() + "affected");
//        } catch (SQLException e) {
//            System.err.println("Error while updating an article, article_id: " + article_id);
//            e.printStackTrace();
//        } finally {
//            close();
//        }
//    }


    @Override
    public void close() {
        mySQL.close();
    }
}

