package DAO;

import DAO.Models.Account;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AccountDAO implements AutoCloseable {

    Connection connector;
    String query, query2;
    ConnectionMySQL mySQL;

    Account account;


    public AccountDAO(String config) {

        connector = null;
        query = null;
        mySQL = new ConnectionMySQL(config);
        account = new Account();
    }

    
    //   ***   for admin pack   ***
    public List<HashMap<String, String>> getAllUsersByRoleID(int type_id) {
        
        connector = mySQL.establish();
        List<HashMap<String, String>> listUsers = new ArrayList<>();

        query = "select a.id, login, firstname, lastname, email from account a, account_type r where a.id = r.id and r.type_id = ?";
        
        try {
            PreparedStatement pst = connector.prepareStatement(query);
            pst.setString(1, String.valueOf(type_id));
            ResultSet rs = pst.executeQuery();
            rs.next();
            do {
                HashMap<String, String> data = new HashMap();
                data.put("id", rs.getString(1));
                data.put("login", rs.getString(2));
                data.put("firstname", rs.getString(3));
                data.put("lastname", rs.getString(4));
                data.put("email", rs.getString(5));
                listUsers.add(data);
            } while (rs.next());

            rs.close();
            pst.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close();
        }
        return listUsers;
    }


    public String getUserRoleByID(int id) {

        query = "select account_type.type_id from account_type where id = ?";
        String user_id;

        try {
            PreparedStatement pst = connector.prepareStatement(query);
            pst.setString(1, String.valueOf(id));
            ResultSet rs = pst.executeQuery();
            rs.next();
            user_id = rs.getString(1);

            rs.close();
            pst.close();

        } catch (SQLException e) {
            user_id = "-1";
            e.printStackTrace();
        }
        return user_id;
    }


    public int pushData(Map<String, String> data) {

        connector = mySQL.establish();

        query = "insert into account (login, firstname, lastname, middlename, password_hash, email, birthday, country, avatar_path, about) VALUES (?,?,?,?,?,?,?,?,?,?)";
        query2 = "";

        try {
            PreparedStatement pst = connector.prepareStatement(query);
//            pst.setInt(1, Integer.valueOf(data.get("id")));
            pst.setString(1, data.get("username"));
            pst.setString(2, data.get("firstname"));
            pst.setString(3, data.get("lastname"));
            pst.setString(4, data.get("middlename"));
            pst.setString(5, data.get("password_hash"));
            pst.setString(6, data.get("email"));
            pst.setString(7, data.get("birthday"));
            pst.setString(8, data.get("country"));
            pst.setString(9, data.get("file"));
            pst.setString(10, data.get("description"));


            System.err.println(pst.executeUpdate() + " row(s) affected in account");
            pst.close();

            int id = IDbyLogin(data.get("username"));
//
            query = "insert into account_type (id, type_id, description) values (?, ?, ?)";

//               ***   create user accounts -> put description into account_type database.
//                     user account is type_id = 2

            pst = connector.prepareStatement(query);
            pst.setString(1, String.valueOf(id));
            pst.setString(2, String.valueOf(2));
            pst.setString(3, String.valueOf("user"));

            boolean account_id = pst.execute();
            if (account_id)
                System.err.println("account_id table updated");

//            System.out.println(id);

            pst.close();

        } catch (SQLException e) {
            e.printStackTrace();
            close();
        }
        close();

        //need to return user id

        return IDbyLogin(data.get("username"));
    }


    public HashMap<String, String> getInfoByID(int id) {

        HashMap<String, String> info = new HashMap<>();
        connector = mySQL.establish();

        query = "select login, firstname, lastname, middlename, about, email from account where id = ?";

        try {
            PreparedStatement pst = connector.prepareStatement(query);
            pst.setString(1, String.valueOf(id));
            pst.execute();
            ResultSet rs = pst.executeQuery();
            rs.next();

            info.put("login", rs.getString(1));
            info.put("firstname", rs.getString(2));
            info.put("lastname", rs.getString(3));
            info.put("middlename", rs.getString(4));
            info.put("about", rs.getString(5));
            info.put("email", rs.getString(6));

            rs.close();
            pst.close();


        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close();
        }

        return info;

    }

    // Updates account info
    // last param is actual login we want to change, the first param can be same
    // or can be different if we want to change login name
    public int updateAccount(String login, String firstname, String lastname, String middlename, String email, String about) {

        connector = mySQL.establish();
        int i = 0;

        try {
            query = "update account set firstname = ?, lastname = ?, middlename = ?, email = ?, about = ? where login = ?";
            PreparedStatement pst = connector.prepareStatement(query);
            pst.setString(1, firstname);
            pst.setString(2, lastname);
            pst.setString(3, middlename);
            pst.setString(4, email);
            pst.setString(5, about);
            pst.setString(6, login);

            i = pst.executeUpdate();

            pst.close();

        } catch (SQLException e) {
            close();
            System.err.println("Error while updating account info.");
            e.printStackTrace();
        } finally {
            closeUtil(i);
        }
        return i;

    }

    public int deleteAccount(int id) {


        //   ***   Clean account information from tables in the following order
        //         1) passwords_specs, 2) account_type, 3) account

        connector = mySQL.establish();
        int i = 0;

        try {

            query = "delete from password_specs where id = ?";
            PreparedStatement pst = connector.prepareStatement(query);
            pst.setInt(1, id);
            i = pst.executeUpdate();

            query = "delete from account_type where id = ?";
            pst = connector.prepareStatement(query);
            pst.setInt(1, id);
            i += pst.executeUpdate();

            query = "delete from account where id = ?";
            pst = connector.prepareStatement(query);
            pst.setInt(1, id);
            i += pst.executeUpdate();

            pst.close();

        } catch (SQLException e) {
            System.out.println("Error while deleting an account with an id: " + id);
            e.printStackTrace();
        } finally {
            closeUtil(i);
        }
        return i;
    }

    private void closeUtil(int i) {
        if (i > 0)
            System.err.println("User data successfully changed.");
        System.err.println(i + "row(s) updated.");
        close();
    }

    public int IDbyLogin(String login) {
        connector = mySQL.establish();
        query = "select id from account where login = ?";
        int id = 0;
        try {
            PreparedStatement pst = connector.prepareStatement(query);
            pst.setString(1, login);
            ResultSet rs = pst.executeQuery();
            rs.next();
            id = rs.getInt(1);

            rs.close();
            pst.close();

//            close();
        } catch (SQLException e) {
//            e.printStackTrace();
//            close();
        }
        return id;
    }

    // UTIL - check if account in database
    public boolean isAccountExist(String account) {
        connector = mySQL.establish();
        query = "select login from account where login = ?";
        try {
            PreparedStatement pst = connector.prepareStatement(query);
            pst.setString(1, account);
            ResultSet rs = pst.executeQuery();
            rs.next();
            String login = rs.getString(1);

            rs.close();
            pst.close();

            if (login.equals("")) return false;
            else return true;
        } catch (SQLException e) {
            System.err.println("No account " + account + " found in the database.");
//            e.printStackTrace();
        } finally {
//            close();
        }
        return false;
    }

    public String getHashedPassword(String login) {
//        connector = mySQL.establish();
        query = "select password_hash from account where login = ?";
        String hashedPassword = "";

        try {
            PreparedStatement pst = connector.prepareStatement(query);
            pst.setString(1, login);
            ResultSet rs = pst.executeQuery();
            rs.next();
            hashedPassword = rs.getString(1);

            rs.close();
            pst.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
//            close();
        }
        return hashedPassword;
    }
	
	    public String getAvatarPath (String login){

        String avatarPathTemp = "";

        connector = mySQL.establish();
        query = "select avatar_path from mtro205.account WHERE ? = login";


        try {
            PreparedStatement prep = connector.prepareStatement(query);
            prep.setString(1, login);

            try (ResultSet result = prep.executeQuery()) {

                while (result.next()){

                    String avatarPath = result.getString(1);
                    avatarPathTemp = avatarPath;
                }
            }


        } catch (SQLException e){
            e.printStackTrace();
        }

        return avatarPathTemp;


    }

    @Override
    public void close() {
        mySQL.close();
    }
}
